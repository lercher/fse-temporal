package cache

import (
	"time"

	"gitlab.com/lercher/fse-temporal/syntax"
)

type item struct {
	modtime time.Time
	wf      *syntax.Workflow
}
