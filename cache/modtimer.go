package cache

import "time"

// ModTimer has a ModTime function like the result of os.Stat
type ModTimer interface {
	ModTime() time.Time // modification time
}
