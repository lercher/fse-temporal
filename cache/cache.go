// Package cache provides parsed FSE DSLs on Open and Stat from a filesystem
package cache

import (
	"io"
	"os"
	"sort"
	"sync"

	"gitlab.com/lercher/fse-temporal/listener"
	"gitlab.com/lercher/fse-temporal/syntax"
)

// OpenFunc is a simpler os.Open
type OpenFunc func(fn string) (io.ReadCloser, error)

// StatFunc is a simpler os.Stat where only ModTime() is required
type StatFunc func(fn string) (ModTimer, error)

// New creates a new Cache with os.Open and os.Stat
func New() *Cache {
	return NewWith(
		func(fn string) (io.ReadCloser, error) {
			return os.Open(fn)
		},
		func(fn string) (ModTimer, error) {
			return os.Stat(fn)
		},
	)
}

// NewWith creates a new Cache with custom Open and Stat functions
func NewWith(open OpenFunc, stat StatFunc) *Cache {
	return &Cache{
		open: open,
		stat: stat,
		m:    make(map[string]item),
	}
}

// Cache stores file based DSLs cached by Stat information and indexed on filename
type Cache struct {
	open OpenFunc
	stat StatFunc
	m    map[string]item
	mu   sync.RWMutex
}

// Workflow returns a parsed Workflow which is the same until the unterlying file has a different ModTime
func (c *Cache) Workflow(fn string) (*syntax.Workflow, []listener.Diagnostic, error) {
	// get modTime(fn) to t
	fi, err := c.stat(fn)
	if err != nil {
		return nil, nil, err
	}
	t := fi.ModTime()

	// now read lock the map c.m
	// and lookup fn. It's done, if we find an item with timestamp t
	c.mu.RLock()
	if it, ok := c.m[fn]; ok {
		if it.modtime.Equal(t) {
			c.mu.RUnlock()
			return it.wf, nil, nil
		}
	}
	c.mu.RUnlock()
	// nothing found or different modTime

	// now write lock the map c.m, to put a new version to the cache
	c.mu.Lock()
	defer c.mu.Unlock()
	// try to read again from the map, b/c another go routine could have modified it
	if it, ok := c.m[fn]; ok {
		if it.modtime.Equal(t) {
			return it.wf, nil, nil
		}
	}

	// t can be different now
	fi, err = c.stat(fn)
	if err != nil {
		return nil, nil, err
	}
	t = fi.ModTime()

	// t can be different from f's modTime here as well if we are unlucky.
	// We only experience an extra cache miss next Get, however, i.e. a little extra delay
	// which is acceptable
	f, err := c.open(fn)
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()

	// Parse the wf
	wf, diag, err := syntax.Parse(f, fn)
	if err != nil {
		return nil, diag, err
	}
	it := item{
		modtime: t,
		wf:      wf,
	}
	c.m[fn] = it
	return wf, nil, nil
}

// Workflows makes a snapshot of all cached workflow definitions sorted by filename
func (c *Cache) Workflows() []*syntax.Workflow {
	c.mu.Lock()
	defer c.mu.Unlock()

	keys := make([]string, 0, len(c.m))
	for k := range c.m {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	list := make([]*syntax.Workflow, 0, len(c.m))
	for _, k := range keys {
		list = append(list, c.m[k].wf)
	}
	return list
}
