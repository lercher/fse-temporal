module gitlab.com/lercher/fse-temporal

go 1.16

require (
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/antlr4-go/antlr/v4 v4.13.0
	github.com/fsnotify/fsnotify v1.7.0
	github.com/gorilla/handlers v1.5.2
	github.com/opentracing-contrib/go-stdlib v1.0.0
	github.com/opentracing/opentracing-go v1.2.0
	github.com/uber/jaeger-client-go v2.30.0+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible
	github.com/yuin/goldmark v1.6.0
	github.com/zenazn/goji v1.0.1
	go.temporal.io/api v1.7.1-0.20220326004856-88a7c92fb8b4
	go.temporal.io/sdk v1.14.1-0.20220322131744-55cf584de5c7
)
