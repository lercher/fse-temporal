package wfsclient

import "time"

// HistoryEvent is a simplified copy of a
// temporal.io HistoryEvent protobuf struct
type HistoryEvent struct {
	// see %homedir%\go\pkg\mod\go.temporal.io\api@v1.4.0\history\v1\message.pb.go:3325
	EventID    int64                  `json:"event_id,omitempty"`
	EventTime  *time.Time             `json:"event_time,omitempty"`
	EventType  EventType              `json:"event_type,omitempty"`
	Version    int64                  `json:"version,omitempty"`
	TaskID     int64                  `json:"task_id,omitempty"`
	Attributes map[string]interface{} `json:"Attributes,omitempty"`
}

// taken from %homedir%\go\pkg\mod\go.temporal.io\api@v1.4.0\enums\v1\event_type.pb.go

// EventType is the type of event happened. It determines the Attribute Schema of a HistoryEvent
type EventType int32

const (
	EVENT_TYPE_UNSPECIFIED                                          EventType = 0
	EVENT_TYPE_WORKFLOW_EXECUTION_STARTED                           EventType = 1
	EVENT_TYPE_WORKFLOW_EXECUTION_COMPLETED                         EventType = 2
	EVENT_TYPE_WORKFLOW_EXECUTION_FAILED                            EventType = 3
	EVENT_TYPE_WORKFLOW_EXECUTION_TIMED_OUT                         EventType = 4
	EVENT_TYPE_WORKFLOW_TASK_SCHEDULED                              EventType = 5
	EVENT_TYPE_WORKFLOW_TASK_STARTED                                EventType = 6
	EVENT_TYPE_WORKFLOW_TASK_COMPLETED                              EventType = 7
	EVENT_TYPE_WORKFLOW_TASK_TIMED_OUT                              EventType = 8
	EVENT_TYPE_WORKFLOW_TASK_FAILED                                 EventType = 9
	EVENT_TYPE_ACTIVITY_TASK_SCHEDULED                              EventType = 10
	EVENT_TYPE_ACTIVITY_TASK_STARTED                                EventType = 11
	EVENT_TYPE_ACTIVITY_TASK_COMPLETED                              EventType = 12
	EVENT_TYPE_ACTIVITY_TASK_FAILED                                 EventType = 13
	EVENT_TYPE_ACTIVITY_TASK_TIMED_OUT                              EventType = 14
	EVENT_TYPE_ACTIVITY_TASK_CANCEL_REQUESTED                       EventType = 15
	EVENT_TYPE_ACTIVITY_TASK_CANCELED                               EventType = 16
	EVENT_TYPE_TIMER_STARTED                                        EventType = 17
	EVENT_TYPE_TIMER_FIRED                                          EventType = 18
	EVENT_TYPE_TIMER_CANCELED                                       EventType = 19
	EVENT_TYPE_WORKFLOW_EXECUTION_CANCEL_REQUESTED                  EventType = 20
	EVENT_TYPE_WORKFLOW_EXECUTION_CANCELED                          EventType = 21
	EVENT_TYPE_REQUEST_CANCEL_EXTERNAL_WORKFLOW_EXECUTION_INITIATED EventType = 22
	EVENT_TYPE_REQUEST_CANCEL_EXTERNAL_WORKFLOW_EXECUTION_FAILED    EventType = 23
	EVENT_TYPE_EXTERNAL_WORKFLOW_EXECUTION_CANCEL_REQUESTED         EventType = 24
	EVENT_TYPE_MARKER_RECORDED                                      EventType = 25
	EVENT_TYPE_WORKFLOW_EXECUTION_SIGNALED                          EventType = 26
	EVENT_TYPE_WORKFLOW_EXECUTION_TERMINATED                        EventType = 27
	EVENT_TYPE_WORKFLOW_EXECUTION_CONTINUED_AS_NEW                  EventType = 28
	EVENT_TYPE_START_CHILD_WORKFLOW_EXECUTION_INITIATED             EventType = 29
	EVENT_TYPE_START_CHILD_WORKFLOW_EXECUTION_FAILED                EventType = 30
	EVENT_TYPE_CHILD_WORKFLOW_EXECUTION_STARTED                     EventType = 31
	EVENT_TYPE_CHILD_WORKFLOW_EXECUTION_COMPLETED                   EventType = 32
	EVENT_TYPE_CHILD_WORKFLOW_EXECUTION_FAILED                      EventType = 33
	EVENT_TYPE_CHILD_WORKFLOW_EXECUTION_CANCELED                    EventType = 34
	EVENT_TYPE_CHILD_WORKFLOW_EXECUTION_TIMED_OUT                   EventType = 35
	EVENT_TYPE_CHILD_WORKFLOW_EXECUTION_TERMINATED                  EventType = 36
	EVENT_TYPE_SIGNAL_EXTERNAL_WORKFLOW_EXECUTION_INITIATED         EventType = 37
	EVENT_TYPE_SIGNAL_EXTERNAL_WORKFLOW_EXECUTION_FAILED            EventType = 38
	EVENT_TYPE_EXTERNAL_WORKFLOW_EXECUTION_SIGNALED                 EventType = 39
	EVENT_TYPE_UPSERT_WORKFLOW_SEARCH_ATTRIBUTES                    EventType = 40
)

var eventTypeName = map[EventType]string{
	0:  "Unspecified",
	1:  "WorkflowExecutionStarted",
	2:  "WorkflowExecutionCompleted",
	3:  "WorkflowExecutionFailed",
	4:  "WorkflowExecutionTimedOut",
	5:  "WorkflowTaskScheduled",
	6:  "WorkflowTaskStarted",
	7:  "WorkflowTaskCompleted",
	8:  "WorkflowTaskTimedOut",
	9:  "WorkflowTaskFailed",
	10: "ActivityTaskScheduled",
	11: "ActivityTaskStarted",
	12: "ActivityTaskCompleted",
	13: "ActivityTaskFailed",
	14: "ActivityTaskTimedOut",
	15: "ActivityTaskCancelRequested",
	16: "ActivityTaskCanceled",
	17: "TimerStarted",
	18: "TimerFired",
	19: "TimerCanceled",
	20: "WorkflowExecutionCancelRequested",
	21: "WorkflowExecutionCanceled",
	22: "RequestCancelExternalWorkflowExecutionInitiated",
	23: "RequestCancelExternalWorkflowExecutionFailed",
	24: "ExternalWorkflowExecutionCancelRequested",
	25: "MarkerRecorded",
	26: "WorkflowExecutionSignaled",
	27: "WorkflowExecutionTerminated",
	28: "WorkflowExecutionContinuedAsNew",
	29: "StartChildWorkflowExecutionInitiated",
	30: "StartChildWorkflowExecutionFailed",
	31: "ChildWorkflowExecutionStarted",
	32: "ChildWorkflowExecutionCompleted",
	33: "ChildWorkflowExecutionFailed",
	34: "ChildWorkflowExecutionCanceled",
	35: "ChildWorkflowExecutionTimedOut",
	36: "ChildWorkflowExecutionTerminated",
	37: "SignalExternalWorkflowExecutionInitiated",
	38: "SignalExternalWorkflowExecutionFailed",
	39: "ExternalWorkflowExecutionSignaled",
	40: "UpsertWorkflowSearchAttributes",
}

func (et EventType) String() string {
	return eventTypeName[et]
}
