package wfsclient

// ContextualMap must be provided for create and signal operations,
// it contains context information to be passed through to all function calls
// in the workflow interpreter
type ContextualMap map[string]string
