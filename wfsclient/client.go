// Package wfsclient establishes the communication between client program and the
// FSE workflow API server wfs via http.
package wfsclient

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gitlab.com/lercher/fse-temporal/exec"
	"gitlab.com/lercher/fse-temporal/syntax"
)

// Client decribes an http request/response connection to the FSE wf API server
// located at BaseURL. The API succeeds with empty lists and values
// using the zero Client without doing any actual communication.
type Client struct {
	BaseURL string // BaseURL must not end with a slash
}

func (c Client) isZero() bool {
	return c.BaseURL == ""
}

func (c Client) do(ctx context.Context, operation string, data url.Values, result interface{}) error {
	bodyReader := strings.NewReader(data.Encode())
	url := c.BaseURL + operation
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bodyReader)
	if err != nil {
		return fmt.Errorf("wf api new request %T: %v", result, err)
	}

	// Header for form encoded values
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	// Header for opentracing
	span := opentracing.SpanFromContext(ctx)
	ext.SpanKindRPCClient.Set(span)
	span.Tracer().Inject(span.Context(), opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(req.Header))

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("wf api request %T: %v", result, err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		b, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return fmt.Errorf("wf api status response %T: %v", result, res.Status)
		}
		return fmt.Errorf("wf api status response %T, %v: %v", result, res.Status, string(b))
	}

	if result == nil {
		// no result object for terminate, so we don't need to deserialize anything
		return nil
	}

	t := io.TeeReader(res.Body, ioutil.Discard) // ioutil.Discard | os.Stdout
	dec := json.NewDecoder(t)
	err = dec.Decode(result)
	if err != nil {
		return fmt.Errorf("wf api decoding %T: %v", result, err)
	}

	return nil
}

// List lists all registered workflows
func (c Client) List(ctx context.Context, BOID string, option string, rolenames []string) ([]exec.WFState, error) {
	if c.isZero() {
		return nil, nil
	}

	data := make(url.Values, 3)
	data.Add("boid", BOID)
	data.Add("option", option)
	data["rolename"] = rolenames

	var list []exec.WFState
	err := c.do(ctx, "/list", data, &list)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Schema lists all registered workflows
func (c Client) Schema(ctx context.Context, option string, rolenames []string) ([]exec.WFWithStates, error) {
	if c.isZero() {
		return nil, nil
	}

	data := make(url.Values, 2)
	data.Add("option", option)
	data["rolename"] = rolenames

	var list []exec.WFWithStates
	err := c.do(ctx, "/schema", data, &list)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Signal sends a signal to an FSE workflow instance to trigger an active event.
// The ContextualMap cm is passed to the interpreter and finally to all function calls (i.e. workflow actions) called from it.
func (c Client) Signal(ctx context.Context, workflowID, signal string, rolenames []string, cm ContextualMap) (exec.WFState, error) {
	if c.isZero() {
		return exec.WFState{}, nil
	}

	data := make(url.Values, 3+len(cm))
	data.Add("id", workflowID)
	data.Add("signal", signal)
	data["rolename"] = rolenames
	for k, v := range cm {
		data.Set("X-"+k, v)
	}

	var item exec.WFState
	err := c.do(ctx, "/signal", data, &item)
	if err != nil {
		return exec.WFState{}, err
	}

	return item, nil
}

// Inspect returns the state description of an FSE workflow instance
func (c Client) Inspect(ctx context.Context, workflowID string, rolenames []string) (exec.WFState, error) {
	if c.isZero() {
		return exec.WFState{}, nil
	}

	data := make(url.Values, 2)
	data.Add("id", workflowID)
	data["rolename"] = rolenames

	var item exec.WFState
	err := c.do(ctx, "/inspect", data, &item)
	if err != nil {
		return exec.WFState{}, err
	}

	return item, nil
}

// InspectSchema returns the state description of an FSE workflow instance and the WF Type's schema
func (c Client) InspectSchema(ctx context.Context, workflowID string, rolenames []string) (exec.WFStateWithSchema, error) {
	nilSchema := exec.WFStateWithSchema{Schema: &syntax.Workflow{}}

	if c.isZero() {
		return nilSchema, nil
	}

	data := make(url.Values, 2)
	data.Add("id", workflowID)
	data["rolename"] = rolenames

	var item exec.WFStateWithSchema
	err := c.do(ctx, "/inspectschema", data, &item)
	if err != nil {
		return nilSchema, err
	}

	return item, nil
}

// Create creates a new FSE workflow instance, via a workflow id and a task queue name
// obtained via List. It is not an error to Create an ID twice, however at most only one instance
// is beeing executed. It returns the workflowID and the temporal runID identifying the particular,
// currently executing instance.
// The ContextualMap cm is passed to the interpreter and finally to all function calls (i.e. workflow actions) called from it.
func (c Client) Create(ctx context.Context, workflowID, taskQueue string, cm ContextualMap) (wfID, runID string, err error) {
	if c.isZero() {
		return workflowID, "", nil
	}

	data := make(url.Values, 2+len(cm))
	data.Add("id", workflowID)
	data.Add("taskqueue", taskQueue)
	for k, v := range cm {
		data.Set("X-"+k, v)
	}

	var item struct {
		ID    string `json:"id,omitempty"`
		RunID string `json:"runid,omitempty"`
	}
	err = c.do(ctx, "/create", data, &item)
	if err != nil {
		return workflowID, "", err
	}

	return item.ID, item.RunID, nil
}

// History returns the replayable temporal history of an FSE workflow instance
func (c Client) History(ctx context.Context, workflowID string, rolenames []string) ([]HistoryEvent, error) {
	if c.isZero() {
		return nil, nil
	}

	data := make(url.Values, 2)
	data.Add("id", workflowID)
	data["rolename"] = rolenames

	var list []HistoryEvent
	err := c.do(ctx, "/history", data, &list)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// Terminate immediately terminates a running FSE workflow instance within a temporal worker.
// The parameter reason can be empty. In this case, a default reason is propagated to temporal.
func (c Client) Terminate(ctx context.Context, workflowID, reason string) error {
	if c.isZero() {
		return nil
	}

	data := make(url.Values, 2)
	data.Add("id", workflowID)
	data.Add("reason", reason)

	return c.do(ctx, "/terminate", data, nil)
}
