package reg

import (
	"sort"

	"gitlab.com/lercher/fse-temporal/option"
	"gitlab.com/lercher/fse-temporal/syntax"
	"go.temporal.io/sdk/workflow"
)

type regmap map[Common]Type

// filtered returns Types filtered by the given predicate filter and sorted by WorkflowName
func filteredAndSorted(m regmap, filter func(s Type) bool) []Type {
	var list []Type
	for _, st := range m {
		if filter(st) {
			list = append(list, st)
		}
	}
	sort.Slice(list, func(i, j int) bool {
		// order by WorkflowName asc, Version desc
		// relevant for newestOnly, don't change light-heartedly
		if list[i].WorkflowName == list[j].WorkflowName {
			return list[i].Version > list[j].Version
		}
		return list[i].WorkflowName < list[j].WorkflowName
	})
	return list
}

// newestVersionOnly takes a sorted list as returned by filtered() and returns only the first wf of a name
func newestVersionOnly(ts []Type) (list []Type) {
	name := syntax.Unversioned("")
	for i := range ts {
		if ts[i].WorkflowName != name {
			name = ts[i].WorkflowName
			list = append(list, ts[i])
		}
	}
	return list
}

// Registry is a singleton workflow worker function
func Registry(ctx workflow.Context) error {
	var err error
	logger := workflow.GetLogger(ctx)

	m := make(regmap)

	err = workflow.SetQueryHandler(ctx, QueryWorkflowWorkflowsAdmin, func() ([]Type, error) {
		logger.Info("query all workflows")
		return filteredAndSorted(m, func(s Type) bool { return true }), nil
	})
	if err != nil {
		return err
	}

	err = workflow.SetQueryHandler(ctx, QueryWorkflowWorkflowsFor, func(opt string, roles []string) ([]Type, error) {
		logger.Info("registry wf query workflows for", "option", opt, "roles", roles)
		o, err := option.Parse(opt)
		if err != nil {
			return nil, err
		}
		list := filteredAndSorted(m, func(t Type) bool { return option.Match(t.OptionParsed(), o) && t.IntersectsWith(roles) })
		list = newestVersionOnly(list)
		return list, nil
	})
	if err != nil {
		return err
	}

	err = workflow.SetQueryHandler(ctx, QueryWorkflowGetWorkflow, func(co Common) ([]Type, error) {
		logger.Info("registry wf get workflow", "common", co)
		list := filteredAndSorted(m, func(s Type) bool { return s.Common == co })
		return list, nil
	})
	if err != nil {
		return err
	}

	regChannel := workflow.GetSignalChannel(ctx, SignalWorkflowRegister)
	n := 0
	for {
		n++
		logger.Info("registry wf iteration", "n", n)
		s := workflow.NewSelector(ctx)
		s.AddReceive(regChannel, func(c workflow.ReceiveChannel, more bool) {
			var t Type
			c.Receive(ctx, &t)
			logger.Info("registry wf register", "type", t.WorkflowName)
			m[t.Common] = t
		})
		s.Select(ctx)
	}
}
