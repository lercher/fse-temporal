package reg

import (
	"gitlab.com/lercher/fse-temporal/option"
	"gitlab.com/lercher/fse-temporal/syntax"
)

// Common is the combined key for an FSE workflow description.
// All other fields of Start are dependent of this properties in terms
// of beeing registered with the Registry temporal workflow.
// In v1 this is the Workflow name only.
type Common struct {
	WorkflowName syntax.Unversioned `json:"workflowName,omitempty"`
}

// Dependent are automatically comparable attributes of an FSE workflow description.
// Workflows have an optional Version string. Any non empty Version is always newer than
// the empty Version and a lexicographically larger string is considered to be a newer version.
type Dependent struct {
	Version     syntax.Unversioned `json:"version,omitempty"`
	Option      string             `json:"option,omitempty"`
	Display     string             `json:"display,omitempty"`
	Description string             `json:"description,omitempty"`
	TaskQueue   string             `json:"taskQueue,omitempty"`
}

// OptionParsed returns Option parsed as option.Option or empty options if invalid
func (d Dependent) OptionParsed() option.Option {
	o, _ := option.Parse(d.Option)
	return o
}

// Type describes an FSE workflow
type Type struct {
	Common
	Dependent
	Roles      []string    `json:"roles,omitempty"`
	Statenames []Statename `json:"statenames,omitempty"`
}

// Statename describes a workflow state by key and display text
type Statename struct {
	Name    string `json:"name,omitempty"`
	Display string `json:"display,omitempty"`
}

// Equals is true, if all attributes match and the Roles slices are index-wise equal
func (s Type) Equals(s2 Type) bool {
	if s.Common != s2.Common || s.Dependent != s2.Dependent || len(s.Roles) != len(s2.Roles) {
		return false
	}
	for i := range s.Roles {
		if s.Roles[i] != s2.Roles[i] {
			return false
		}
	}
	return true
}

// IntersectsWith is true if e.Roles is empty or
// otherwise if there is a non-empty intersection between e.Roles
// and the given roles
func (s Type) IntersectsWith(roles []string) bool {
	if len(s.Roles) == 0 {
		// empty "for" list matches all roles
		return true
	}
	for _, r1 := range s.Roles {
		for _, r2 := range roles {
			if r1 == r2 {
				return true
			}
		}
	}
	return false
}
