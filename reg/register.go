package reg

import (
	"context"
	"fmt"

	"gitlab.com/lercher/fse-temporal/syntax"
	"go.temporal.io/api/enums/v1"
	"go.temporal.io/sdk/client"
)

// Register queries for a Type type, if it doesn't exist or is different, replace it.
// This function also starts the Registry workflow if there is no instance of it.
func Register(ctx context.Context, cli client.Client, s Type) error {
	existing, runID, err := workflows(ctx, cli, QueryWorkflowWorkflowsAdmin)
	if err != nil {
		return err
	}

	// if there is none or it differs, signal the new Type
	if len(existing) == 0 || !existing[0].Equals(s) {
		err = cli.SignalWorkflow(ctx, WorkflowID, runID, SignalWorkflowRegister, s)
		if err != nil {
			return fmt.Errorf("signal registry workflow: %v", err)
		}
	}
	return nil
}

// Versioned returns the newest Version of a particular workflow type, if it doesn't exist, an error is returned
func Versioned(ctx context.Context, cli client.Client, workflowName syntax.Unversioned) (syntax.Versioned, error) {
	types, err := Versions(ctx, cli, workflowName)
	if err != nil {
		return "", err
	}
	if len(types) == 0 {
		err = fmt.Errorf("no such registered fse workflow type: %q", workflowName)
		return "", err
	}
	return syntax.VersionedName(workflowName, types[0].Version), nil
}

// Versions returns all Versions sorted newer to older of a particular workflow type
func Versions(ctx context.Context, cli client.Client, workflowName syntax.Unversioned) ([]Type, error) {
	co := Common{WorkflowName: workflowName}
	list, _, err := workflows(ctx, cli, QueryWorkflowGetWorkflow, co)
	return list, err
}

// WorkflowsOf returns all currently registered workflows filtered by option and a list of roles
func WorkflowsOf(ctx context.Context, cli client.Client, option string, roles []string) ([]Type, error) {
	list, _, err := workflows(ctx, cli, QueryWorkflowWorkflowsFor, option, roles)
	return list, err
}

// Workflows returns all currently registered workflows
func Workflows(ctx context.Context, cli client.Client) ([]Type, error) {
	list, _, err := workflows(ctx, cli, QueryWorkflowWorkflowsAdmin)
	return list, err
}

// workflows returns the queried types and the RunID of the registry execution
func workflows(ctx context.Context, cli client.Client, queryType string, args ...interface{}) ([]Type, string, error) {
	run, err := cli.ExecuteWorkflow(ctx, client.StartWorkflowOptions{
		ID:                    WorkflowID,
		TaskQueue:             TaskQueue,
		WorkflowIDReusePolicy: enums.WORKFLOW_ID_REUSE_POLICY_ALLOW_DUPLICATE_FAILED_ONLY,
	},
		Registry,
	)
	if err != nil {
		// it's no error, if there is a WorkflowIDReusePolicy violation
		return nil, "", fmt.Errorf("execute registry wf: %v", err)
	}

	// try to get s from the registry WF
	runID := run.GetRunID()
	val, err := cli.QueryWorkflow(ctx, WorkflowID, runID, queryType, args...)
	if err != nil {
		return nil, runID, fmt.Errorf("query registry wf %s/%s: %v", WorkflowID, runID, err)
	}
	var existing []Type
	err = val.Get(&existing)
	if err != nil {
		return nil, runID, fmt.Errorf("query registry wf get value: %v", err)
	}

	return existing, run.GetRunID(), nil
}
