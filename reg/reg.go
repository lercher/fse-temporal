// Package reg implements a temporal.io workflow singleton worker
// and query functions to keep a global registry of all available
// fse workflow types
package reg

const (
	// QueryWorkflowWorkflowsAdmin lists all known workflows of all versions (admin mode). It has no parameters.
	QueryWorkflowWorkflowsAdmin = "workflows"
	// QueryWorkflowWorkflowsFor lists all newest versions of workflows for the given option and roles. It has two parameters: string and []string.
	QueryWorkflowWorkflowsFor = "workflows-for"
	// QueryWorkflowGetWorkflow lists zero or all workflows of the given Common ordered by Version descending. It has one parameter: Common.
	QueryWorkflowGetWorkflow = "get-workflow"
	// SignalWorkflowRegister is the signal's name to be passed to SignalWorkflow to register a new fse workflow type
	SignalWorkflowRegister = "register"
)

const (
	// TaskQueue is temporal's task queue name for hosting this Registry workflow
	TaskQueue = "_FSERegistry"
	// WorkflowID is this Registry workflow's ID as a singleton
	WorkflowID = "_FSEGlobal"
)
