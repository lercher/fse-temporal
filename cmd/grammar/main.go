package main

// go run . -j sample.fse > sample.json

import (
	"encoding/json"
	"flag"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/lercher/fse-temporal/syntax"
	"gitlab.com/lercher/fse-temporal/tpl"
)

var (
	flagJSON = flag.Bool("j", false, "output parsed JSON to stdout (can't be mixed with -h)")
	flagHTML = flag.Bool("h", false, "output an HTML page describing the workflow to stdout (can't be mixed with -j)")
)

func main() {
	log.Println("This is grammar, see https://gitlab.com/lercher/fse-temporal for details")
	os.Exit(main2())
}

func main2() int {
	defer func(t time.Time) {
		log.Println(time.Now().Sub(t), "total processing time")
	}(time.Now())

	flag.Parse()

	var r io.Reader
	var path string
	switch {
	case flag.NArg() > 0 && flag.Arg(0) != "-":
		path = flag.Arg(0)
		f, err := os.Open(path)
		if err != nil {
			log.Println(err)
			return 1
		}
		defer f.Close()
		r = f
	default:
		path = ""
		r = os.Stdin
	}

	wf, diag, err := syntax.Parse(r, path)
	if err != nil {
		for i := range diag {
			log.Println(diag[i])
		}
		log.Println(err)
		return 2
	}

	log.Printf("%s %q loaded successfully", wf.Name, wf.Display)
	log.Printf("%v total states, %v top level states", len(wf.AllStates), len(wf.States))
	logStates(wf.States, 0)
	switch {
	case *flagJSON:
		enc := json.NewEncoder(os.Stdout)
		enc.SetIndent("", "  ")
		err = enc.Encode(wf)
		if err != nil {
			log.Println(err)
			return 3
		}

	case *flagHTML:
		err := tpl.TemplateItem(wf).Execute(os.Stdout, wf)
		if err != nil {
			log.Println(err)
			return 5
		}
	}

	return 0
}

func logState(s syntax.State, level int) {
	evt := make([]string, len(s.Events))
	for i := range s.Events {
		switch {
		case s.Events[i].Type == "final-event":
			evt[i] = "#" + s.Events[i].NameOrDisplay()
		default:
			evt[i] = s.Events[i].NameOrDisplay()
		}
	}
	log.Printf("%[1]*[2]s %-[3]*[4]s %v", level, "", 40-level, s.Name, evt)
	logStates(s.States, level+2)
}

func logStates(ss []syntax.State, level int) {
	for i := range ss {
		logState(ss[i], level)
	}
}
