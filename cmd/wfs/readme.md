# FSE DSL API Server

The API server hosts an http endpoint that provides lists
for startable and already running workflow instances for
particular business objects represented by an option string and
and ID. Furthermore operations to create, inspect, signal and terminate
instances. To run it:

```sh
go run .
```

The API server listens on `http://localhost:9002/...` by default
where `...` is one of the paths [`list create inspect signal terminate`].
The listening port can be set with the `-p <port>` command line option.

## FSE DSI Workflows

The API Server looks for `*.fse` files in the [wf](wf) subdirectory by default.
If there are none it refuses to start. The location and the wildcard pattern
can be changed with the `-w <glob-pattern>` command line option.

## Registry Workflow Worker

The API server also hosts a temporal workflow worker
for the singleton registry workflow that keeps track
of all available FSE workflow descriptions.

**Note:** FSE DSL
files are discovered and registerd with the Registry workflow
at the API server's startup only. They are
cached in memory until an FSE DSL file changes its modification
time. That means that you can modify an already existing FSE DSL
file which is then used for newly created instances but not add or
delete existing ones. If you do so, you need to restart the
API server process.

**Warning:** please be aware that changing an FSE DSL workflow
substantially easily produces a lot of errors in the temporal
workflow worker due to beeing non reproducible. It is
advisable to version FSE DSL workflows by their name and keep older
ones available until there are no more running instances.

## Common Notes for All Operations

All parameters can be transferred via URL query
parameters or via standard form post encoding. The
http method is ignored, however you should use
only `GET` and `POST` as indicated in the following sections.
Jaeger Tracing compatible Trace/Span ID http headers are recognized,
if there are none, new spans are created.

### Parameter List `rolename`

There is one parameter list
called `rolename`, to transfer multiple values you need to include
`rolename=sth` multiple times with the same key `rolename`.

### Extra, Contextual Parameters `X-*` for `POST`-operations

This section applies only to post-ed operations `create` and `signal`.

Passed parameters via URL query or form parameters with a name
starting with `X-` are recognized as so called *contextual parameters*.
They are collected in a `map[string]string` by striping the prefix `X-`
from the name unsing that as the map key and putting the first value into the map.

For example the parameter `X-userid=this-user` puts `this-user` into the
contextual map at `userid`. A byte slice with the UTF-8 encoded
JSON string `{"userid":"this-user"}` is then passed to all actions called
by the execution engine.

On creation the map is passed to the fse workflow execution engine as Memo which
propagates the values serialized as JSON to all action calls. On signaling
the map is passed directly as JSON to each action call, i.e. WF Memo values are not
mutated. For timer events, i.e. using the `after 30s` syntax in the FSE
definition file, as there is no context, the initial contextual map is passed
to actions called as consequences of the timer event.

All contextual parameters are added as properties to the current
OpenTracing span.

### Workflow ID Schema

Workflow IDs adhere to this particular schema:

FSE Workflow Name | dot | Business Object Name | dot | Business Object ID
---|---|---|---|---
`simple-workflow` | `.` | `client` | `.` | `client-123`

I.e. in the example above `simple-workflow.client.client-123`

### HTTP Responses

Responses are as explaind in the following section:

* either non-200-OK status codes plus a text message explaining the error
* or a JSON stream containing the requested data
* or empty

## Operation `schema`

Schema looks up all available FSE workflow types matching with the
given option parameter and available for the given list of rolenames.
It then reports these types with key, display name and option string
as well as all available states with key and display name
ordered by their position in the fse file.

|| Kind | Use | Description
---|---|---|---
`/schema` | URL Path | mandatory | header object
`GET` | http method | mandatory | header object
`option` | parameter | mandatory | [option-string](../../option/option.go) that is used to [match](../../option/match.go) with an FSE DSL's workflow option. `bo` is required like in `bo=Client; subtype=C1 C2;`
`rolename`* | parameter list | mandatory | only FSE workflows with an Intersecting role list, as specified by `for <role>...` in the [DSL](../../FSE.g4), are included in the response.

### JSON Success Response for `schema`

`[]exec.WFWithStates` - list of workflow types and their states.

## Operation `list`

List looks up all available FSE workflow types matching with the
given option parameter and available for the given list of rolenames.
For each such type it then queries for running temporal instances
of the given boid and bo (within option). If there is a running instance
its state replaces the type information in the list, which is returned otherwise.

This procedure gives the illusion, that every registered
type of FSE-workflow is uniquely active for each business object,
some of which appear not yet started,
some are running (technically speaking, there is indeed an open instance)
and some already completed or were terminated.

|| Kind | Use | Description
---|---|---|---
`/list` | URL Path | mandatory | header object
`GET` | http method | mandatory | header object
`boid` | parameter | mandatory | busines object's id like `client-123`
`option` | parameter | mandatory | [option-string](../../option/option.go) that is used to [match](../../option/match.go) with an FSE DSL's workflow option. `bo` is required like in `bo=Client; subtype=C1 C2;`
`rolename`* | parameter list | mandatory | only FSE workflows with an Intersecting role list, as specified by `for <role>...` in the [DSL](../../FSE.g4), are included in the response.

### JSON Success Response for `list`

`[]exec.WFState` - list of open or createable workflow instances.

Pick the ID for `inspect`, `signal` and `terminate`.

Pick ID and TaskQueue for `create`.

## Operation `create`

Create starts a new temporal workflow instance by ID. It is no error
to create a workflow instance with the same ID multiple times, only
one instance is created, however.

|| Kind | Use | Description
---|---|---|---
`/create` | URL Path | mandatory | header object
`POST` | http method | mandatory | header object
`id` | parameter | mandatory | workflow ID which was provided by `list`
`taskqueue` | parameter | mandatory | temporal's task-queue name which was provided by `list`
`X-*` | parameter | optional | contextual information as described above, these values are stored and used as contextual for timer events

### JSON Success Response for `create`

Create returns this information, which can be ignored most of the time.
It is an echo of the ID given and a RunID provided by the temporal server,
it is somthing like an instance ID of a particular temporal workflow.

```JSON
{
  "id": "other-workflow.ADAdresse.12ADA3456",
  "runid": "0ae9c1e8-d5b2-45c0-9d8a-e315938e8672"
}
```

## Operation `inspect`

Inspect returns information on a running (`Alive == true`) FSE workflow
instance identified by ID.

|| Kind | Use | Description
---|---|---|---
`/inspect` | URL Path | mandatory | header object
`GET` | http method | mandatory | header object
`id` | parameter | mandatory | workflow ID which was provided by `list`
`rolename`* | parameter list | mandatory | see `list`

### JSON Success Response for `inspect`

`exec.WFState` - workflow instance's description including
current state and available signals and an optional time-out deadline
for an automatic state transition.

## Operation `history`

History returns temporal replay history of an FSE workflow
instance identified by ID.

|| Kind | Use | Description
---|---|---|---
`/history` | URL Path | mandatory | header object
`GET` | http method | mandatory | header object
`id` | parameter | mandatory | workflow ID which was provided by `list`
`rolename`* | parameter list | mandatory | see `list`

### JSON Success Response for `history`

`"go.temporal.io/api/history/v1" []*history.HistoryEvent` - workflow instance's
history of events suitable for doing a workflow replay.
See <https://community.temporal.io/t/go-sdk-troubleshooting/4440#replaying-11>
for more details on replaying history.

## Operation `signal`

Signal triggers an FSE workflow event by name. If successful the
request is transferred to `inspect` which returns the
FSE workflow's next current state.

|| Kind | Use | Description
---|---|---|---
`/signal` | URL Path | mandatory | header object
`POST` | http method | mandatory | header object
`id` | parameter | mandatory | workflow ID which was provided by `list`
`signal` | parameter | mandatory | name of the FSE workflow's event to trigger, the event's name can be picked from an `inspect` or a previous `signal`call.
`rolename`* | parameter list | mandatory | see `list`
`X-*` | parameter | optional | contextual information to be passed as contextual map JSON to action calls executed as consequences of the signal processing

### JSON Success Response for `signal`

See `inspect` - if `signal` was successful it responds as if `inspect` was called.

## Operation `terminate`

Terminate a workflow instance immediately.

|| Kind | Use | Description
---|---|---|---
`/signal` | URL Path | mandatory | header object
`POST` | http method | mandatory | header object
`id` | parameter | mandatory | workflow ID which was provided by `list`
`reason` | parameter | optional | reason for terminating the instance, if missing `terminated by user request` is passed to temporal

### Empty Success Response for `terminate`

Terminate uses only the http status to signal success.

## Example Web Page

You can open [root.html](root.html) in your favourite browser
and try out some API operations. Be sure to start the API
server *with default port* up-front:

```sh
go run . -temporal localhost:7233
```

assuming a standard local temporal.io installation, see [temporal.yaml](../../temporal-28.yaml)

## On Extending the WFS

Serving workflows by definition files alone is quite boring without calling any actions.
And this is what this wfs provides only. However there are two methods for adding
actions to the interpreted eco-system: using the wfs as a plugin to some program
hosting temporal.io actions *or* adding actions as standalone processes communicating with
the temporal.io server.

### Hosting Actions in Standalone Processes

This approach should be straight-forward in terms of temporal.io's architecture,
i.e.:

* hook up a temporal API client
* register some actions, carefully selecting compatible action queue names
* have them do some work, i.e. start processes hosting actions
* start this wfs directing it to some *.fse files and have it publish action
  requests on the action queue name selected (see `-aq` cmd line flag)

### Using wfs as a Plug-In

Starting with tag `v0.5.4` there is the option to host wfs in your own process by
importing `"gitlab.com/lercher/fse-temporal/wfs"`, creating some proper `wfs.ServerOptions`
and calling `InitializeServer()` on them. However it is the hosting program's responsibility
to:

* provide a connected temporal API client
* initialize a proper `wfs.ServerOptions` struct
* use all (or only some) http handlers of the `wfs.Server` created by `InitializeServer()`
* create, start and stop an http/s server
* call the server's `Close()` method when done

For details have a look at [main.go](main.go). It's probably a good idea
to copy main.go, delete what's un-wanted or un-needed and add some own
action worker stuff to it.
