package main

// go run . -temporal linux-home:7233 -f simple.fse
// go run . -temporal tumble:7233     -f simple.fse

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitlab.com/lercher/fse-temporal/wfs"

	"github.com/gorilla/handlers"
	"github.com/zenazn/goji/web/middleware"

	"go.temporal.io/sdk/client"
)

var ( // this program's options
	flagHostPort = flag.String("temporal", "tumble:7233", "host:port of a temporal.io instance, the standard port is 7233. Use dns:/// prefix to enable DNS based round-robin")
	flagPort     = flag.Int("p", 9002, "host the API on http://localhost:this-port/")
)

var ( // wfs.ServerOptions
	flagFSE           = flag.String("w", "wf/*.fse", "host all temporal workflows defined by this glob pattern (see also -watch)")
	flagServicename   = flag.String("n", "lsWFS", "service name for open tracing")
	flagTaskQueue     = flag.String("q", "fse", "Task queue name for this process' temporal workflows and activities, i.e. where clients post tasks to to reach this process")
	flagWatch         = flag.Duration("watch", 500*time.Millisecond, "add a file system watcher for fse workflow files to restart workflows on any change, if positive. duration is debounce time (see also -w)")
	flagResetRegistry = flag.Bool("reset", false, "setting this flag terminates and then restarts the global workflows registry workflow with ID _FSEGlobal")
)

func main() {
	flag.Parse()

	so := wfs.ServerOptions{
		WorkflowsGlobPattern:   *flagFSE,
		WatcherDebounce:        *flagWatch,
		ResetRegistry:          *flagResetRegistry,
		TaskqueueName:          *flagTaskQueue,
		OpenTracingServicename: *flagServicename,
	}

	// The client is a heavyweight temporal object that should be created once
	serviceClient, err := client.NewClient(client.Options{
		HostPort: *flagHostPort,
	})
	if err != nil {
		log.Fatalln("service client:", err)
	}
	defer serviceClient.Close()

	s, err := so.InitializeServer(serviceClient, nil) // nil: we don't have custom activities here. If true they need to be added through a non-nil callback here.
	if err != nil {
		closeErr := s.Close()
		if closeErr != nil {
			log.Println(closeErr)
		}
		log.Fatalln(err)
	}

	handler := s.AddHandlers(http.NewServeMux())
	handler = handlers.CompressHandler(handler) // gorilla gzip support
	handler = middleware.NoCache(handler)       // using no-cache headers

	var p = fmt.Sprintf(":%d", *flagPort)
	srv := &http.Server{Addr: p, Handler: handler}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs

		log.Println("Stopping http server ...")
		err := srv.Shutdown(context.Background()) // causes srv.ListenAndServe() to return which falls into select{} to halt the main go routine
		if err != nil {
			log.Println(err)
		}

		err = s.Close()
		if err != nil {
			log.Println(err)
		}

		log.Println("Done.")
		os.Exit(0)
	}()

	log.Println()
	log.Println("Have a look at", "http://"+strings.Split(*flagHostPort, ":")[0]+":8088/", "for the temporal admin UI")
	log.Println("The API server listens on", fmt.Sprintf("http://localhost%s/", p), "plus the api paths [schema list create inspect signal terminate]. Press ^C to stop the server.")
	log.Println()

	// start web server
	// always returns error. ErrServerClosed on graceful close
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// unexpected error. port in use?
		log.Fatalf("ListenAndServe: %v", err)
	}
	select {} // wait forever, i.e. until os.Exit(0) is called in the go routine above
}
