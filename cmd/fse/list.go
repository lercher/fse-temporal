package main

import (
	"context"
	"fmt"
	"time"

	filterV1 "go.temporal.io/api/filter/v1"
	workflowV1 "go.temporal.io/api/workflow/v1"
	workflowserviceV1 "go.temporal.io/api/workflowservice/v1"
	"go.temporal.io/sdk/client"
)

// adapted from https://github.com/temporalio/go-samples/blob/master/searchattributes/searchattributes_workflow.go
// adapted from https://github.com/temporalio/temporal/blob/master/tools/cli/workflowCommands.go

var sinceEver = time.Time{}

func listExecutions(ctx context.Context, serviceClient client.Client, wfname string) ([]*workflowV1.WorkflowExecutionInfo, error) {
	now := time.Now()
	req := &workflowserviceV1.ListOpenWorkflowExecutionsRequest{
		MaximumPageSize: 100,
		StartTimeFilter: &filterV1.StartTimeFilter{
			EarliestTime: &sinceEver,
			LatestTime:   &now,
		},
	}
	if wfname != "" {
		// Types that are valid to be assigned to Filters:
		//    *workflowserviceV1.ListOpenWorkflowExecutionsRequest_ExecutionFilter -> WorkflowId/string, RunId/string
		//    *workflowserviceV1.ListOpenWorkflowExecutionsRequest_TypeFilter -> Name/string
		req.Filters = &workflowserviceV1.ListOpenWorkflowExecutionsRequest_TypeFilter{
			TypeFilter: &filterV1.WorkflowTypeFilter{
				Name: wfname,
			},
		}
	}

	var list []*workflowV1.WorkflowExecutionInfo
	for {
		resp, err := serviceClient.ListOpenWorkflow(ctx, req)
		if err != nil {
			return nil, fmt.Errorf("serviceClient.ListOpenWorkflow: %v", err)
		}
		list = append(list, resp.Executions...)
		if len(resp.NextPageToken) == 0 {
			break // no more pages
		}
		req.NextPageToken = resp.NextPageToken
	}

	return list, nil
}
