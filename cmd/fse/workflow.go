package main

import (
	"gitlab.com/lercher/fse-temporal/cache"
	"gitlab.com/lercher/fse-temporal/exec"

	"go.temporal.io/sdk/workflow"
)

func makeWorkflow(c *cache.Cache, fn string, actionsTaskQueue string) func(ctx workflow.Context) error {
	return func(ctx workflow.Context) error {
		logger := workflow.GetLogger(ctx)
		wf, diag, err := c.Workflow(fn)
		if err != nil {
			for i := range diag {
				logger.Info(diag[i].String()) // diag[i].String() already contains the file name
			}
			return err
		}

		in := exec.New(ctx, wf, nil)
		in.ActivityOptions.TaskQueue = actionsTaskQueue
		return in.Run(nil)
	}
}
