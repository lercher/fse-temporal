package watcher

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestClean(t *testing.T) {
	if got, want := filepath.Clean("./a.x"), "a.x"; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
	if got, want := filepath.Clean("a.x"), "a.x"; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

func TestEmptyWatcher(t *testing.T) {
	w, err := New("wontExist*.xyz", 500*time.Millisecond)
	if err == nil {
		t.Fatal("expected an error, got", err)
	}
	_ = w
}

func TestNewWatcherEarlyClose(t *testing.T) {
	b, _ := os.Create("b.x")
	b.Close()

	w, err := New("*.x", 5000*time.Millisecond)
	if err != nil {
		t.Fatal(err)
	}
	count := 0
	go func() {
		for range w.Changed {
			t.Log("change")
			count++
		}
	}()
	_ = ioutil.WriteFile("a.x", []byte{65, 66, 67}, 0)
	_ = ioutil.WriteFile("c.y", []byte{65, 66, 67, 68}, 0)
	_ = ioutil.WriteFile("a.x", []byte{65, 66, 67, 68, 69}, 0)

	for i := 0; i < 80; i++ {
		t.Log(i*25, "...")
		<-time.After(25 * time.Millisecond) // enough time for an fs event but before debounce 2s
		if atomic.AddInt32(&w.countEvents, 0) != 0 {
			break
		}
	}
	w.Close()
	_ = os.Remove("a.x")
	_ = os.Remove("b.x")
	_ = os.Remove("c.y")

	<-time.After(50 * time.Millisecond)
	if count != 1 {
		t.Errorf("want count %v, got %v", 1, count)
	}
}

func TestNewWatcherLateClose(t *testing.T) {
	b, _ := os.Create("b.x")
	b.Close()

	w, err := New("*.x", 100*time.Millisecond)
	if err != nil {
		t.Fatal(err)
	}

	mu := new(sync.Mutex)
	mu.Lock()
	count := 0
	go func() {
		for range w.Changed {
			t.Log("change")
			count++
			mu.Unlock()
		}
	}()
	_ = ioutil.WriteFile("a.x", []byte{65, 66, 67}, 0)
	_ = ioutil.WriteFile("c.y", []byte{65, 66, 67, 68}, 0)
	_ = ioutil.WriteFile("a.x", []byte{65, 66, 67, 68, 69}, 0)

	mu.Lock()
	w.Close()
	_ = os.Remove("a.x")
	_ = os.Remove("b.x")
	_ = os.Remove("c.y")

	if count != 1 {
		t.Errorf("want count %v, got %v", 1, count)
	}
}
