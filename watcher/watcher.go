package watcher

import (
	"errors"
	"log"
	"os"
	"path/filepath"
	"sync"
	"sync/atomic"
	"time"

	"github.com/fsnotify/fsnotify"
)

// Watcher watches existing os directories that contain Pattern matches for infrequent file changes
// like manual saving or git-like operations.
// Directories created after calling New are not watched for FS changes.
// If FS changes are detected that match Pattern, one "true" is sent to Changed not before Debounce.
// Changes within the Debounce interval retrigger the Debounce pause, so frequent
// and continued FS changes will probably never trigger any sending to Changed.
// Watcher's file system watching routine blocks, if no go routine is listening to Changed.
// The Changed channel is closed when Close() is called so you can range over Changed.
type Watcher struct {
	Pattern     string
	Changed     chan bool
	Debounce    time.Duration
	countEvents int32 // countEvents is only needed by a test. It is 32bit, b/c an int64 caused "panic: unaligned 64-bit atomic operation" at atomic.AddInt64(this value) on 32bit Win7
	mu          sync.Mutex
	w           *fsnotify.Watcher
}

// New returns a new Watcher that watches all already existing dirs of all os files matching with globPattern
func New(globPattern string, debounce time.Duration) (*Watcher, error) {
	w, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}
	watch := &Watcher{
		Pattern:  globPattern,
		Changed:  make(chan bool),
		Debounce: debounce,
		w:        w,
	}

	m, err := filepath.Glob(globPattern)
	if err != nil {
		w.Close()
		return nil, err
	}
	dirs := make(map[string]bool)
	for _, fn := range m {
		stat, err := os.Stat(fn)
		if err != nil {
			log.Println(err)
			continue // ignore the file stat error
		}
		dir := ""
		if stat.IsDir() {
			dir = fn
		} else {
			dir, _ = filepath.Split(fn)
		}
		if dirs[dir] {
			continue // already watched
		}
		dirs[dir] = true
		err = w.Add(dir)
		if err != nil {
			w.Close()
			return nil, err
		}
		if dir == "" {
			dir, _ = os.Getwd()
		}
		log.Println("watching directory", dir)
	}
	if len(dirs) == 0 {
		w.Close()
		return nil, errors.New("empty directory watcher is not allowed")
	}

	go watch.rangeErrors()
	go watch.rangeEvents()

	watch.mu.Lock()
	return watch, nil
}

func (w *Watcher) rangeErrors() {
	for err := range w.w.Errors {
		log.Println("directory watcher error:", err)
	}
}

func (w *Watcher) rangeEvents() {
	debounce := make(chan bool)

	go func() {
		var after <-chan time.Time
		for {
			select {
			case <-after:
				after = nil
				w.Changed <- true
			case _, ok := <-debounce:
				if !ok {
					if after != nil {
						after = nil
						w.Changed <- true
					}
					close(w.Changed)
					w.mu.Unlock()
					return
				}
				after = time.After(w.Debounce) // start or restart an intervall
			}
		}
	}()

	for event := range w.w.Events {
		// Windows "a.x", Linux "./a.x", so we clean it
		fn := filepath.Clean(event.Name)
		matched, err := filepath.Match(w.Pattern, fn)
		if err != nil {
			log.Println("directory watcher", event, "match error:", err)
			continue
		}
		if matched {
			log.Println("directory watcher debouncing match", event, "...")
			// it's unlikely to overflow: 2^31 is a large number of file system changes
			atomic.AddInt32(&w.countEvents, 1)
			debounce <- true
		} else {
			log.Println("directory watcher event ignored:", event)
		}
	}
	close(debounce)
}

// Close closes the underlying file system watcher
func (w *Watcher) Close() error {
	err := w.w.Close()
	w.mu.Lock()
	return err
}
