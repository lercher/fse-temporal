package trace

import (
	"io"
	"log"

	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-lib/metrics"

	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
)

// Initialize sets up an FSE DSL standard tracer and makes it global.
// It is the responsibility of the caller to Close the returned closer, if no error was returned.
// Jaeger is basically configured from environment vars, as listed
// on https://github.com/jaegertracing/jaeger-client-go#environment-variables
// However we override the servicename and set the sampler to every span.
func Initialize(servicename string, lg *log.Logger) (opentracing.Tracer, io.Closer, error) {
	cfg, err := jaegercfg.FromEnv()
	if err != nil {
		return nil, nil, err
	}

	cfg.ServiceName = servicename
	cfg.Sampler.Type = jaeger.SamplerTypeConst
	cfg.Sampler.Param = 1

	// Example logger and metrics factory. Use github.com/uber/jaeger-client-go/log
	// and github.com/uber/jaeger-lib/metrics respectively to bind to real logging and metrics
	// frameworks.
	jLogger := &logger{lg}
	jMetricsFactory := metrics.NullFactory

	// Initialize tracer with a logger and a metrics factory
	tracer, closer, err := cfg.NewTracer(
		jaegercfg.Logger(jLogger),
		jaegercfg.Metrics(jMetricsFactory),
	)

	if err != nil {
		return nil, nil, err
	}

	// Set the singleton opentracing.Tracer with the Jaeger tracer.
	opentracing.SetGlobalTracer(tracer)

	return tracer, closer, nil // need to defer closer.Close() at the caller
}

// ID extraxts the jaeger TraceID as a hex string, which in turn can be fed into the Jaeger UI
func ID(span opentracing.Span) string {
	type traceIDer interface {
		TraceID() jaeger.TraceID
	}
	sc := span.Context()
	if jsc, ok := sc.(traceIDer); ok {
		return jsc.TraceID().String()
	}
	return ""
}
