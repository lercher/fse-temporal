# Open Tracing Support for Pasing

Open Tracing is used to aggregate distributes tracing information
on a Jaeger server. With its GUI a reguest can be traced even
if it hops along multiple services.

## Ressources

* [Golang quickstart](https://opentracing.io/guides/golang/quick-start/)
* [Jaeger homepage](https://www.jaegertracing.io/)
* [Jaeger on Github](https://github.com/jaegertracing/jaeger)
* [Jaeger Agent with http reposting support](https://github.com/Lercher/jaeger/tree/agent-1.18-http)
