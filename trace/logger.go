package trace

import (
	"fmt"
	"log"
)

type logger struct {
	*log.Logger
}

func (l *logger) Error(msg string) {
	l.Logger.Printf("ERROR: %s", msg)
}

// Infof logs a message at info priority
func (l *logger) Infof(msg string, args ...interface{}) {
	l.Logger.Printf(msg, args...)
}

// Debugf logs a message at debug priority
func (l *logger) Debugf(msg string, args ...interface{}) {
	l.Logger.Printf(fmt.Sprintf("DEBUG: %s", msg), args...)
}
