package trace

import (
	"net/http"

	"github.com/opentracing-contrib/go-stdlib/nethttp"
	"github.com/opentracing/opentracing-go"
)

// Middleware returns a gorilla mux compatible http middleware, that extracts from r or starts a new open tracing
// span and puts it into the request's context via opentracing.ContextWithSpan(). Use it by opentracing.SpanFromContext().
// A Pasing-suitable operation name is formed from the request and nethttp Std tags are added to the span.
func Middleware(tr opentracing.Tracer) func(h http.Handler) http.Handler {
	// see https://godoc.org/github.com/opentracing-contrib/go-stdlib/nethttp#MWComponentName
	return func(h http.Handler) http.Handler {
		return nethttp.Middleware(
			tr,
			h,
			nethttp.OperationNameFunc(func(r *http.Request) string {
				return "HTTP " + r.Method + ":" + r.URL.Path
			}),
		)
	}
}
