package trace

import (
	"testing"

	jaegercfg "github.com/uber/jaeger-client-go/config"
)

func TestTraceID(t *testing.T) {
	cfg := jaegercfg.Configuration{
		ServiceName: "servicename",
	}
	tr, closer, err := cfg.NewTracer()
	if err != nil {
		t.Fatal(err)
	}
	defer closer.Close()

	span := tr.StartSpan("nop")

	if got, want := ID(span), ""; got == want {
		t.Errorf("want != %v, got %v", want, got)
	}
	// don't flush
}
