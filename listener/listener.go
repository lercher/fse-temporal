// Package listener adds semantic error handling to a standard antlr.DiagnosticListener
// and conforms to the go error handling infrastructure
package listener

import (
	"fmt"

	"github.com/antlr4-go/antlr/v4"
)

// Listener records and logs syntax errors produced by the antlr4 parser
type Listener struct {
	Path string
	*antlr.DiagnosticErrorListener
	Diagnostics []Diagnostic
}

// New creates a new Listener
func New(path string) *Listener {
	dl := antlr.NewDiagnosticErrorListener(true)
	return &Listener{
		Path:                    path,
		DiagnosticErrorListener: dl,
	}
}

// Error makes Listener itself an error
func (el *Listener) Error() string {
	if el.Path != "" {
		return fmt.Sprintf("%v: %d error(s) detected %v", el.Path, el.ErrorCount(), el.Diagnostics)
	}
	return fmt.Sprintf("%d error(s) detected %v", el.ErrorCount(), el.Diagnostics)
}

// ErrorCount counts the number of Diagnostic messages
func (el *Listener) ErrorCount() int {
	return len(el.Diagnostics)
}

// SyntaxError implements an interface method of antlr.DiagnosticListener
func (el *Listener) SyntaxError(
	recognizer antlr.Recognizer,
	offendingSymbol interface{},
	line, column int,
	msg string,
	e antlr.RecognitionException,
) {
	el.Diagnostics = append(el.Diagnostics, Diagnostic{"syntax", el.Path, line, column, msg})
}

// Semantic Errors

// SemErr logs a semantic error
func (el *Listener) SemErr(
	offendingToken antlr.Token,
	msg string,
) {
	line := offendingToken.GetLine()
	column := offendingToken.GetColumn()
	el.SemanticError(line, column, msg)
}

// SemanticError logs a semantic error
func (el *Listener) SemanticError(
	line, column int,
	msg string,
) {
	el.Diagnostics = append(el.Diagnostics, Diagnostic{"semantic", el.Path, line, column, msg})
}
