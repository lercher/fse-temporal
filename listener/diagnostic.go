package listener

import (
	"fmt"
)

// Diagnostic represents a locatable error in an FSE spec
type Diagnostic struct {
	Type    string
	Path    string
	Line    int
	Column  int
	Message string
}

func (d Diagnostic) String() string {
	if d.Path != "" {
		return fmt.Sprintf("%s error %v:%d:%d %s", d.Type, d.Path, d.Line, d.Column, d.Message)
	}
	return fmt.Sprintf("%s error line %d:%d %s", d.Type, d.Line, d.Column, d.Message)
}
