go mod tidy && go fmt ./... && go vet -unreachable=false ./... && go test ./... && go build ./cmd/fse && go build ./cmd/grammar && go build ./cmd/registry-worker && go build ./cmd/wfs
