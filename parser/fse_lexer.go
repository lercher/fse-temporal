// Code generated from c://git//src//gitlab.com//lercher//fse-temporal//FSE.g4 by ANTLR 4.13.1. DO NOT EDIT.

package parser

import (
	"fmt"
	"github.com/antlr4-go/antlr/v4"
	"sync"
	"unicode"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = sync.Once{}
var _ = unicode.IsLetter

type FSELexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var FSELexerLexerStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	ChannelNames           []string
	ModeNames              []string
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func fselexerLexerInit() {
	staticData := &FSELexerLexerStaticData
	staticData.ChannelNames = []string{
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
	}
	staticData.ModeNames = []string{
		"DEFAULT_MODE",
	}
	staticData.LiteralNames = []string{
		"", "'workflow'", "'version'", "':'", "'as'", "'for'", "','", "'state'",
		"'{'", "'}'", "'initialize'", "'after'", "'event'", "'final-event'",
		"';'", "'$$'", "'='", "'('", "')'", "'if'", "'=='", "'!='", "'goto'",
		"'halt'", "'terminate'",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		"", "", "", "", "", "", "", "", "COMMENT", "LINE_COMMENT", "WS", "NAME",
		"VAR", "TEXT", "DURATION", "DESCRIPTION", "ERRORCHARACTER",
	}
	staticData.RuleNames = []string{
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8",
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16",
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "COMMENT",
		"LINE_COMMENT", "WS", "IDENTIFIER", "NAME", "VAR", "TEXT", "NUMBER",
		"DURATION", "DESCRIPTION", "ERRORCHARACTER",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 0, 33, 285, 6, -1, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2,
		4, 7, 4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2,
		10, 7, 10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7, 13, 2, 14, 7, 14, 2, 15,
		7, 15, 2, 16, 7, 16, 2, 17, 7, 17, 2, 18, 7, 18, 2, 19, 7, 19, 2, 20, 7,
		20, 2, 21, 7, 21, 2, 22, 7, 22, 2, 23, 7, 23, 2, 24, 7, 24, 2, 25, 7, 25,
		2, 26, 7, 26, 2, 27, 7, 27, 2, 28, 7, 28, 2, 29, 7, 29, 2, 30, 7, 30, 2,
		31, 7, 31, 2, 32, 7, 32, 2, 33, 7, 33, 2, 34, 7, 34, 1, 0, 1, 0, 1, 0,
		1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		1, 1, 1, 1, 1, 2, 1, 2, 1, 3, 1, 3, 1, 3, 1, 4, 1, 4, 1, 4, 1, 4, 1, 5,
		1, 5, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 7, 1, 7, 1, 8, 1, 8, 1, 9,
		1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 10, 1, 10,
		1, 10, 1, 10, 1, 10, 1, 10, 1, 11, 1, 11, 1, 11, 1, 11, 1, 11, 1, 11, 1,
		12, 1, 12, 1, 12, 1, 12, 1, 12, 1, 12, 1, 12, 1, 12, 1, 12, 1, 12, 1, 12,
		1, 12, 1, 13, 1, 13, 1, 14, 1, 14, 1, 14, 1, 15, 1, 15, 1, 16, 1, 16, 1,
		17, 1, 17, 1, 18, 1, 18, 1, 18, 1, 19, 1, 19, 1, 19, 1, 20, 1, 20, 1, 20,
		1, 21, 1, 21, 1, 21, 1, 21, 1, 21, 1, 22, 1, 22, 1, 22, 1, 22, 1, 22, 1,
		23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 24,
		1, 24, 1, 24, 1, 24, 5, 24, 189, 8, 24, 10, 24, 12, 24, 192, 9, 24, 1,
		24, 1, 24, 1, 24, 1, 24, 1, 24, 1, 25, 1, 25, 1, 25, 3, 25, 202, 8, 25,
		1, 25, 5, 25, 205, 8, 25, 10, 25, 12, 25, 208, 9, 25, 1, 25, 1, 25, 1,
		26, 1, 26, 1, 26, 1, 26, 1, 27, 1, 27, 3, 27, 218, 8, 27, 1, 27, 5, 27,
		221, 8, 27, 10, 27, 12, 27, 224, 9, 27, 1, 27, 1, 27, 5, 27, 228, 8, 27,
		10, 27, 12, 27, 231, 9, 27, 1, 27, 3, 27, 234, 8, 27, 1, 28, 1, 28, 1,
		29, 1, 29, 1, 29, 1, 30, 1, 30, 1, 30, 1, 30, 5, 30, 245, 8, 30, 10, 30,
		12, 30, 248, 9, 30, 1, 30, 1, 30, 1, 31, 1, 31, 5, 31, 254, 8, 31, 10,
		31, 12, 31, 257, 9, 31, 1, 31, 1, 31, 4, 31, 261, 8, 31, 11, 31, 12, 31,
		262, 3, 31, 265, 8, 31, 1, 32, 1, 32, 1, 32, 1, 33, 1, 33, 1, 33, 1, 33,
		1, 33, 5, 33, 275, 8, 33, 10, 33, 12, 33, 278, 9, 33, 1, 33, 1, 33, 1,
		33, 1, 33, 1, 34, 1, 34, 3, 190, 229, 276, 0, 35, 1, 1, 3, 2, 5, 3, 7,
		4, 9, 5, 11, 6, 13, 7, 15, 8, 17, 9, 19, 10, 21, 11, 23, 12, 25, 13, 27,
		14, 29, 15, 31, 16, 33, 17, 35, 18, 37, 19, 39, 20, 41, 21, 43, 22, 45,
		23, 47, 24, 49, 25, 51, 26, 53, 27, 55, 0, 57, 28, 59, 29, 61, 30, 63,
		0, 65, 31, 67, 32, 69, 33, 1, 0, 9, 2, 0, 10, 10, 13, 13, 3, 0, 9, 10,
		13, 13, 32, 32, 2, 0, 65, 90, 97, 122, 2, 0, 45, 45, 95, 95, 3, 0, 48,
		57, 65, 90, 97, 122, 3, 0, 10, 10, 13, 13, 34, 34, 1, 0, 49, 57, 1, 0,
		48, 57, 3, 0, 104, 104, 109, 109, 115, 115, 295, 0, 1, 1, 0, 0, 0, 0, 3,
		1, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 9, 1, 0, 0, 0, 0, 11,
		1, 0, 0, 0, 0, 13, 1, 0, 0, 0, 0, 15, 1, 0, 0, 0, 0, 17, 1, 0, 0, 0, 0,
		19, 1, 0, 0, 0, 0, 21, 1, 0, 0, 0, 0, 23, 1, 0, 0, 0, 0, 25, 1, 0, 0, 0,
		0, 27, 1, 0, 0, 0, 0, 29, 1, 0, 0, 0, 0, 31, 1, 0, 0, 0, 0, 33, 1, 0, 0,
		0, 0, 35, 1, 0, 0, 0, 0, 37, 1, 0, 0, 0, 0, 39, 1, 0, 0, 0, 0, 41, 1, 0,
		0, 0, 0, 43, 1, 0, 0, 0, 0, 45, 1, 0, 0, 0, 0, 47, 1, 0, 0, 0, 0, 49, 1,
		0, 0, 0, 0, 51, 1, 0, 0, 0, 0, 53, 1, 0, 0, 0, 0, 57, 1, 0, 0, 0, 0, 59,
		1, 0, 0, 0, 0, 61, 1, 0, 0, 0, 0, 65, 1, 0, 0, 0, 0, 67, 1, 0, 0, 0, 0,
		69, 1, 0, 0, 0, 1, 71, 1, 0, 0, 0, 3, 80, 1, 0, 0, 0, 5, 88, 1, 0, 0, 0,
		7, 90, 1, 0, 0, 0, 9, 93, 1, 0, 0, 0, 11, 97, 1, 0, 0, 0, 13, 99, 1, 0,
		0, 0, 15, 105, 1, 0, 0, 0, 17, 107, 1, 0, 0, 0, 19, 109, 1, 0, 0, 0, 21,
		120, 1, 0, 0, 0, 23, 126, 1, 0, 0, 0, 25, 132, 1, 0, 0, 0, 27, 144, 1,
		0, 0, 0, 29, 146, 1, 0, 0, 0, 31, 149, 1, 0, 0, 0, 33, 151, 1, 0, 0, 0,
		35, 153, 1, 0, 0, 0, 37, 155, 1, 0, 0, 0, 39, 158, 1, 0, 0, 0, 41, 161,
		1, 0, 0, 0, 43, 164, 1, 0, 0, 0, 45, 169, 1, 0, 0, 0, 47, 174, 1, 0, 0,
		0, 49, 184, 1, 0, 0, 0, 51, 201, 1, 0, 0, 0, 53, 211, 1, 0, 0, 0, 55, 233,
		1, 0, 0, 0, 57, 235, 1, 0, 0, 0, 59, 237, 1, 0, 0, 0, 61, 240, 1, 0, 0,
		0, 63, 251, 1, 0, 0, 0, 65, 266, 1, 0, 0, 0, 67, 269, 1, 0, 0, 0, 69, 283,
		1, 0, 0, 0, 71, 72, 5, 119, 0, 0, 72, 73, 5, 111, 0, 0, 73, 74, 5, 114,
		0, 0, 74, 75, 5, 107, 0, 0, 75, 76, 5, 102, 0, 0, 76, 77, 5, 108, 0, 0,
		77, 78, 5, 111, 0, 0, 78, 79, 5, 119, 0, 0, 79, 2, 1, 0, 0, 0, 80, 81,
		5, 118, 0, 0, 81, 82, 5, 101, 0, 0, 82, 83, 5, 114, 0, 0, 83, 84, 5, 115,
		0, 0, 84, 85, 5, 105, 0, 0, 85, 86, 5, 111, 0, 0, 86, 87, 5, 110, 0, 0,
		87, 4, 1, 0, 0, 0, 88, 89, 5, 58, 0, 0, 89, 6, 1, 0, 0, 0, 90, 91, 5, 97,
		0, 0, 91, 92, 5, 115, 0, 0, 92, 8, 1, 0, 0, 0, 93, 94, 5, 102, 0, 0, 94,
		95, 5, 111, 0, 0, 95, 96, 5, 114, 0, 0, 96, 10, 1, 0, 0, 0, 97, 98, 5,
		44, 0, 0, 98, 12, 1, 0, 0, 0, 99, 100, 5, 115, 0, 0, 100, 101, 5, 116,
		0, 0, 101, 102, 5, 97, 0, 0, 102, 103, 5, 116, 0, 0, 103, 104, 5, 101,
		0, 0, 104, 14, 1, 0, 0, 0, 105, 106, 5, 123, 0, 0, 106, 16, 1, 0, 0, 0,
		107, 108, 5, 125, 0, 0, 108, 18, 1, 0, 0, 0, 109, 110, 5, 105, 0, 0, 110,
		111, 5, 110, 0, 0, 111, 112, 5, 105, 0, 0, 112, 113, 5, 116, 0, 0, 113,
		114, 5, 105, 0, 0, 114, 115, 5, 97, 0, 0, 115, 116, 5, 108, 0, 0, 116,
		117, 5, 105, 0, 0, 117, 118, 5, 122, 0, 0, 118, 119, 5, 101, 0, 0, 119,
		20, 1, 0, 0, 0, 120, 121, 5, 97, 0, 0, 121, 122, 5, 102, 0, 0, 122, 123,
		5, 116, 0, 0, 123, 124, 5, 101, 0, 0, 124, 125, 5, 114, 0, 0, 125, 22,
		1, 0, 0, 0, 126, 127, 5, 101, 0, 0, 127, 128, 5, 118, 0, 0, 128, 129, 5,
		101, 0, 0, 129, 130, 5, 110, 0, 0, 130, 131, 5, 116, 0, 0, 131, 24, 1,
		0, 0, 0, 132, 133, 5, 102, 0, 0, 133, 134, 5, 105, 0, 0, 134, 135, 5, 110,
		0, 0, 135, 136, 5, 97, 0, 0, 136, 137, 5, 108, 0, 0, 137, 138, 5, 45, 0,
		0, 138, 139, 5, 101, 0, 0, 139, 140, 5, 118, 0, 0, 140, 141, 5, 101, 0,
		0, 141, 142, 5, 110, 0, 0, 142, 143, 5, 116, 0, 0, 143, 26, 1, 0, 0, 0,
		144, 145, 5, 59, 0, 0, 145, 28, 1, 0, 0, 0, 146, 147, 5, 36, 0, 0, 147,
		148, 5, 36, 0, 0, 148, 30, 1, 0, 0, 0, 149, 150, 5, 61, 0, 0, 150, 32,
		1, 0, 0, 0, 151, 152, 5, 40, 0, 0, 152, 34, 1, 0, 0, 0, 153, 154, 5, 41,
		0, 0, 154, 36, 1, 0, 0, 0, 155, 156, 5, 105, 0, 0, 156, 157, 5, 102, 0,
		0, 157, 38, 1, 0, 0, 0, 158, 159, 5, 61, 0, 0, 159, 160, 5, 61, 0, 0, 160,
		40, 1, 0, 0, 0, 161, 162, 5, 33, 0, 0, 162, 163, 5, 61, 0, 0, 163, 42,
		1, 0, 0, 0, 164, 165, 5, 103, 0, 0, 165, 166, 5, 111, 0, 0, 166, 167, 5,
		116, 0, 0, 167, 168, 5, 111, 0, 0, 168, 44, 1, 0, 0, 0, 169, 170, 5, 104,
		0, 0, 170, 171, 5, 97, 0, 0, 171, 172, 5, 108, 0, 0, 172, 173, 5, 116,
		0, 0, 173, 46, 1, 0, 0, 0, 174, 175, 5, 116, 0, 0, 175, 176, 5, 101, 0,
		0, 176, 177, 5, 114, 0, 0, 177, 178, 5, 109, 0, 0, 178, 179, 5, 105, 0,
		0, 179, 180, 5, 110, 0, 0, 180, 181, 5, 97, 0, 0, 181, 182, 5, 116, 0,
		0, 182, 183, 5, 101, 0, 0, 183, 48, 1, 0, 0, 0, 184, 185, 5, 47, 0, 0,
		185, 186, 5, 42, 0, 0, 186, 190, 1, 0, 0, 0, 187, 189, 9, 0, 0, 0, 188,
		187, 1, 0, 0, 0, 189, 192, 1, 0, 0, 0, 190, 191, 1, 0, 0, 0, 190, 188,
		1, 0, 0, 0, 191, 193, 1, 0, 0, 0, 192, 190, 1, 0, 0, 0, 193, 194, 5, 42,
		0, 0, 194, 195, 5, 47, 0, 0, 195, 196, 1, 0, 0, 0, 196, 197, 6, 24, 0,
		0, 197, 50, 1, 0, 0, 0, 198, 199, 5, 47, 0, 0, 199, 202, 5, 47, 0, 0, 200,
		202, 5, 35, 0, 0, 201, 198, 1, 0, 0, 0, 201, 200, 1, 0, 0, 0, 202, 206,
		1, 0, 0, 0, 203, 205, 8, 0, 0, 0, 204, 203, 1, 0, 0, 0, 205, 208, 1, 0,
		0, 0, 206, 204, 1, 0, 0, 0, 206, 207, 1, 0, 0, 0, 207, 209, 1, 0, 0, 0,
		208, 206, 1, 0, 0, 0, 209, 210, 6, 25, 0, 0, 210, 52, 1, 0, 0, 0, 211,
		212, 7, 1, 0, 0, 212, 213, 1, 0, 0, 0, 213, 214, 6, 26, 0, 0, 214, 54,
		1, 0, 0, 0, 215, 222, 7, 2, 0, 0, 216, 218, 7, 3, 0, 0, 217, 216, 1, 0,
		0, 0, 217, 218, 1, 0, 0, 0, 218, 219, 1, 0, 0, 0, 219, 221, 7, 4, 0, 0,
		220, 217, 1, 0, 0, 0, 221, 224, 1, 0, 0, 0, 222, 220, 1, 0, 0, 0, 222,
		223, 1, 0, 0, 0, 223, 234, 1, 0, 0, 0, 224, 222, 1, 0, 0, 0, 225, 229,
		5, 91, 0, 0, 226, 228, 9, 0, 0, 0, 227, 226, 1, 0, 0, 0, 228, 231, 1, 0,
		0, 0, 229, 230, 1, 0, 0, 0, 229, 227, 1, 0, 0, 0, 230, 232, 1, 0, 0, 0,
		231, 229, 1, 0, 0, 0, 232, 234, 5, 93, 0, 0, 233, 215, 1, 0, 0, 0, 233,
		225, 1, 0, 0, 0, 234, 56, 1, 0, 0, 0, 235, 236, 3, 55, 27, 0, 236, 58,
		1, 0, 0, 0, 237, 238, 5, 36, 0, 0, 238, 239, 3, 55, 27, 0, 239, 60, 1,
		0, 0, 0, 240, 246, 5, 34, 0, 0, 241, 245, 8, 5, 0, 0, 242, 243, 5, 92,
		0, 0, 243, 245, 5, 34, 0, 0, 244, 241, 1, 0, 0, 0, 244, 242, 1, 0, 0, 0,
		245, 248, 1, 0, 0, 0, 246, 244, 1, 0, 0, 0, 246, 247, 1, 0, 0, 0, 247,
		249, 1, 0, 0, 0, 248, 246, 1, 0, 0, 0, 249, 250, 5, 34, 0, 0, 250, 62,
		1, 0, 0, 0, 251, 255, 7, 6, 0, 0, 252, 254, 7, 7, 0, 0, 253, 252, 1, 0,
		0, 0, 254, 257, 1, 0, 0, 0, 255, 253, 1, 0, 0, 0, 255, 256, 1, 0, 0, 0,
		256, 264, 1, 0, 0, 0, 257, 255, 1, 0, 0, 0, 258, 260, 5, 46, 0, 0, 259,
		261, 7, 7, 0, 0, 260, 259, 1, 0, 0, 0, 261, 262, 1, 0, 0, 0, 262, 260,
		1, 0, 0, 0, 262, 263, 1, 0, 0, 0, 263, 265, 1, 0, 0, 0, 264, 258, 1, 0,
		0, 0, 264, 265, 1, 0, 0, 0, 265, 64, 1, 0, 0, 0, 266, 267, 3, 63, 31, 0,
		267, 268, 7, 8, 0, 0, 268, 66, 1, 0, 0, 0, 269, 270, 5, 60, 0, 0, 270,
		271, 5, 45, 0, 0, 271, 272, 5, 45, 0, 0, 272, 276, 1, 0, 0, 0, 273, 275,
		9, 0, 0, 0, 274, 273, 1, 0, 0, 0, 275, 278, 1, 0, 0, 0, 276, 277, 1, 0,
		0, 0, 276, 274, 1, 0, 0, 0, 277, 279, 1, 0, 0, 0, 278, 276, 1, 0, 0, 0,
		279, 280, 5, 45, 0, 0, 280, 281, 5, 45, 0, 0, 281, 282, 5, 62, 0, 0, 282,
		68, 1, 0, 0, 0, 283, 284, 9, 0, 0, 0, 284, 70, 1, 0, 0, 0, 14, 0, 190,
		201, 206, 217, 222, 229, 233, 244, 246, 255, 262, 264, 276, 1, 0, 1, 0,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// FSELexerInit initializes any static state used to implement FSELexer. By default the
// static state used to implement the lexer is lazily initialized during the first call to
// NewFSELexer(). You can call this function if you wish to initialize the static state ahead
// of time.
func FSELexerInit() {
	staticData := &FSELexerLexerStaticData
	staticData.once.Do(fselexerLexerInit)
}

// NewFSELexer produces a new lexer instance for the optional input antlr.CharStream.
func NewFSELexer(input antlr.CharStream) *FSELexer {
	FSELexerInit()
	l := new(FSELexer)
	l.BaseLexer = antlr.NewBaseLexer(input)
	staticData := &FSELexerLexerStaticData
	l.Interpreter = antlr.NewLexerATNSimulator(l, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	l.channelNames = staticData.ChannelNames
	l.modeNames = staticData.ModeNames
	l.RuleNames = staticData.RuleNames
	l.LiteralNames = staticData.LiteralNames
	l.SymbolicNames = staticData.SymbolicNames
	l.GrammarFileName = "FSE.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// FSELexer tokens.
const (
	FSELexerT__0           = 1
	FSELexerT__1           = 2
	FSELexerT__2           = 3
	FSELexerT__3           = 4
	FSELexerT__4           = 5
	FSELexerT__5           = 6
	FSELexerT__6           = 7
	FSELexerT__7           = 8
	FSELexerT__8           = 9
	FSELexerT__9           = 10
	FSELexerT__10          = 11
	FSELexerT__11          = 12
	FSELexerT__12          = 13
	FSELexerT__13          = 14
	FSELexerT__14          = 15
	FSELexerT__15          = 16
	FSELexerT__16          = 17
	FSELexerT__17          = 18
	FSELexerT__18          = 19
	FSELexerT__19          = 20
	FSELexerT__20          = 21
	FSELexerT__21          = 22
	FSELexerT__22          = 23
	FSELexerT__23          = 24
	FSELexerCOMMENT        = 25
	FSELexerLINE_COMMENT   = 26
	FSELexerWS             = 27
	FSELexerNAME           = 28
	FSELexerVAR            = 29
	FSELexerTEXT           = 30
	FSELexerDURATION       = 31
	FSELexerDESCRIPTION    = 32
	FSELexerERRORCHARACTER = 33
)
