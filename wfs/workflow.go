package wfs

import (
	"log"
	"strings"

	"gitlab.com/lercher/fse-temporal/cache"
	"gitlab.com/lercher/fse-temporal/exec"

	"go.temporal.io/api/common/v1"
	"go.temporal.io/sdk/workflow"
)

func makeWorkflowFunc(c *cache.Cache, fn string, ao workflow.ActivityOptions) func(ctx workflow.Context) error {
	return func(ctx workflow.Context) error {
		logger := workflow.GetLogger(ctx)
		wf, diag, err := c.Workflow(fn)
		if err != nil {
			for i := range diag {
				logger.Info(diag[i].String()) // diag[i].String() already contains the file name
			}
			return err
		}
		memo := workflow.GetInfo(ctx).Memo
		cm := memoToContexualMap(memo)
		in := exec.New(ctx, wf, nil)
		in.ActivityOptions = ao
		return in.Run(cm)
	}
}

func memoToContexualMap(memo *common.Memo) map[string]string {
	m := make(map[string]string, memo.Size())
	for k, payload := range memo.GetFields() {
		v := string(payload.GetData())
		v = strings.Trim(v, `"`)
		v = strings.TrimSpace(v)
		log.Printf("contextual from memo %v=%q", k, v) // TODO: this needs to be checked and probably converted
		m[k] = v
	}
	return m
}
