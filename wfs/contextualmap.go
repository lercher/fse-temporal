package wfs

import (
	"net/http"
	"strings"

	"github.com/opentracing/opentracing-go"
)

func contextualMap(r *http.Request, span opentracing.Span) map[string]string {
	cm := make(map[string]string)

	for k, vs := range r.Form {
		if strings.HasPrefix(k, "X-") && k != "X-" {
			key := strings.TrimPrefix(k, "X-")
			v := strings.Join(vs, "|")
			cm[key] = v
			span.SetTag(k, v) // X-key=v
		}
	}
	return cm
}

func toMemo(m map[string]string) map[string]interface{} {
	mm := make(map[string]interface{}, len(m))
	for k, v := range m {
		mm[k] = v
	}
	return mm
}
