package wfs

import (
	"context"
	"fmt"
	"log"
	"path/filepath"

	"gitlab.com/lercher/fse-temporal/cache"
	"gitlab.com/lercher/fse-temporal/reg"
	"gitlab.com/lercher/fse-temporal/syntax"

	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
	"go.temporal.io/sdk/workflow"
)

func startWorkersFseReg(
	ctx context.Context,
	serviceClient client.Client,
	taskQueue string,
	ao workflow.ActivityOptions,
	c *cache.Cache,
	workflows []*syntax.Workflow,
	add MoreActivities,
) (worker.Worker, worker.Worker, error) {

	regWorker := worker.New(serviceClient, reg.TaskQueue, worker.Options{})
	// add the registry workflow to the worker
	log.Println("adding the registry workflow to worker ...")
	regWorker.RegisterWorkflow(reg.Registry)
	err := regWorker.Start()
	if err != nil {
		return nil, nil, fmt.Errorf("reg worker: %v", err)
	}

	fseWorker := worker.New(serviceClient, taskQueue, worker.Options{})
	// add custom activities to worker
	if add != nil {
		log.Println("adding custom activities to worker ...")
		err := add(fseWorker)
		if err != nil {
			regWorker.Stop()
			return nil, nil, err
		}
	}

	// add all *.fse defined workflows
	ao.TaskQueue = taskQueue
	for _, wf := range workflows {
		log.Println("adding fse workflow:", wf.VersionedName())
		fseWorker.RegisterWorkflowWithOptions(makeWorkflowFunc(c, wf.Path, ao), workflow.RegisterOptions{
			Name: string(wf.VersionedName()),
		})

		// this needs the regWorker be up and running
		err := reg.Register(ctx, serviceClient, wf2type(wf, taskQueue))
		if err != nil {
			regWorker.Stop()
			return nil, nil, fmt.Errorf("register: %v", err)
		}
		log.Println("successfully registered", wf.VersionedName(), "with the registry workflow")
	}

	err = fseWorker.Start()
	if err != nil {
		regWorker.Stop()
		return nil, nil, fmt.Errorf("fse worker: %v", err)
	}

	return fseWorker, regWorker, nil
}

func wf2type(wf *syntax.Workflow, taskQueue string) reg.Type {
	t := reg.Type{
		Common: reg.Common{
			WorkflowName: wf.Name,
		},
		Dependent: reg.Dependent{
			Version:     wf.Version,
			Option:      wf.Option,
			Display:     wf.Display,
			Description: wf.Description,
			TaskQueue:   taskQueue,
		},
		Roles:      wf.Roles,
		Statenames: make([]reg.Statename, len(wf.Statenames)),
	}
	for i := range wf.Statenames {
		t.Statenames[i].Name = wf.Statenames[i].Name.String()
		t.Statenames[i].Display = wf.Statenames[i].Display
	}
	return t
}

func globWorkflows(c *cache.Cache, pattern string) ([]*syntax.Workflow, error) {
	m, err0 := filepath.Glob(pattern)
	if err0 != nil {
		return nil, err0
	}
	var err error
	list := make([]*syntax.Workflow, 0, len(m))
	for _, fn := range m {
		wf, err1 := loadWF(c, fn)
		if err1 != nil {
			err = err1
			continue
		}
		list = append(list, wf)
	}
	return list, err
}

func loadWF(c *cache.Cache, fn string) (*syntax.Workflow, error) {
	wf, diag, err := c.Workflow(fn)
	if err != nil {
		for i := range diag {
			log.Println(fn, diag[i].String())
		}
		return nil, fmt.Errorf("%s: %v", fn, err)
	}

	log.Println("wf loaded successfully from", fn, "-",
		"name:", wf.Name,
		"display:", wf.Display,
		"states:", len(wf.AllStates),
		"toplevelStates:", len(wf.States),
		// "description:", wf.Description,
	)

	return wf, nil
}
