package wfs

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/opentracing/opentracing-go"
	"gitlab.com/lercher/fse-temporal/cache"
	"gitlab.com/lercher/fse-temporal/exec"
	"gitlab.com/lercher/fse-temporal/option"
	"gitlab.com/lercher/fse-temporal/reg"
	"gitlab.com/lercher/fse-temporal/syntax"
	"gitlab.com/lercher/fse-temporal/tpl"
	"gitlab.com/lercher/fse-temporal/trace"
	"gitlab.com/lercher/fse-temporal/watcher"

	enumsV1 "go.temporal.io/api/enums/v1"
	"go.temporal.io/api/history/v1"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

// MoreActivities is a function callback type to add more activities and custom worflows to the framework's single worker
type MoreActivities func(activityWorker worker.Worker) error

// Server operates a workflow registry and a bunch of *.fse workflows stored and watched on disk
type Server struct {
	TemporalClient client.Client      // Client is attached to a temporal.io server
	Cache          *cache.Cache       // Cache holds workflow definitions loaded from disk
	Watcher        *watcher.Watcher   // Watcher is used to watch the file system for *.fse changes
	Tracer         opentracing.Tracer // Tracer is used to trace spans to
	Add            MoreActivities     // Add is used to inject custom activities implemented by client code. If nil this callback method is not called.
	tracecloser    io.Closer          // tracecloser is used to shut down the Tracer
	fseWorker      worker.Worker      // fseWorker is the temporal worker for all hosted *.fse workflows and for all custom activities injected by Add
	regWorker      worker.Worker      // regWorker hosts the registry workflow
}

// Close closes all services of the Server
func (s Server) Close() error {
	if s.Watcher != nil {
		log.Println("Stopping directory watcher ...")
		err := s.Watcher.Close()
		if err != nil {
			return fmt.Errorf("close watcher: %v", err)
		}
	}

	if s.fseWorker != nil {
		log.Println("Stopping fse worker ...")
		func() {
			defer func() {
				r := recover()
				if r != nil {
					log.Println("recovered from panic while stopping fse worker:", r)
				}
			}()
			s.fseWorker.Stop()
		}()
	}

	if s.regWorker != nil {
		log.Println("Stopping registry worker ...")
		func() {
			defer func() {
				r := recover()
				if r != nil {
					log.Println("recovered from panic while stopping registry worker:", r)
				}
			}()
			s.regWorker.Stop()
		}()
	}

	if s.tracecloser != nil {
		log.Println("Stopping tracer ...")
		err := s.tracecloser.Close()
		if err != nil {
			return fmt.Errorf("close tracer: %v", err)
		}
	}

	return nil
}

// watch is started as a goroutine to replace the FSE worker when s.Watcher signals a FS change
func (s Server) watch(so ServerOptions) {
	for range s.Watcher.Changed {
		log.Println("detected FSE file change, restarting worker ...")

		workflows, err := globWorkflows(s.Cache, so.WorkflowsGlobPattern)
		if err != nil {
			log.Println(err)
			continue
		}

		// There is only a race with s.Close() on s.fseWorker
		// any other change on s.fseWorker is only within this range:
		w := s.fseWorker
		if w != nil {
			// w can be nil if startFSEWorker returned an error on last FS change,
			// so we need to guard w.Stop()
			w.Stop()
			s.fseWorker = nil
		}

		rw := s.regWorker
		if rw != nil {
			rw.Stop()
			s.regWorker = nil
		}

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		wrk, regw, err := startWorkersFseReg(ctx, s.TemporalClient, so.TaskqueueName, so.ActivityOptions, s.Cache, workflows, s.Add)
		if err != nil {
			log.Println(err)
			continue
		}
		s.fseWorker, s.regWorker = wrk, regw

		log.Println("fse and registry workers restarted.")
	}
}

// AddHandlers adds all of this Server's http handlers to mux,
// attaches open tracing middleware to mux and returns this handler
func (s Server) AddHandlers(mux *http.ServeMux) http.Handler {
	mux.HandleFunc("/schema", s.Schema)
	mux.HandleFunc("/list", s.List)
	mux.HandleFunc("/create", s.Create)
	mux.HandleFunc("/inspect", s.Inspect)
	mux.HandleFunc("/inspectschema", s.InspectSchema)
	mux.HandleFunc("/signal", s.Signal)
	mux.HandleFunc("/history", s.History)
	mux.HandleFunc("/terminate", s.Terminate)
	mux.HandleFunc("/", s.Definition)

	handler := trace.Middleware(s.Tracer)(mux) // support for opentracing's Inject
	return handler
}

// Schema is the HandlerFunc for the /schema operation
func (s Server) Schema(w http.ResponseWriter, r *http.Request) {
	span0 := opentracing.SpanFromContext(r.Context())

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	Rolenames := r.Form["rolename"] // list
	Option := r.FormValue("option")

	o, err := option.Parse(Option)
	if err != nil {
		err = fmt.Errorf("illegal option string: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}
	BO := o.GetValue("bo")

	span0.SetTag("rolenames", Rolenames).SetTag("option", o.String()).SetTag("bo", BO)

	span := opentracing.StartSpan("registry", opentracing.ChildOf(span0.Context()))
	ts, err := reg.WorkflowsOf(r.Context(), s.TemporalClient, Option, Rolenames)
	if err != nil {
		err = fmt.Errorf("registry: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span.SetTag("error", err.Error()).Finish()
		return
	}
	span.SetTag("count", len(ts))
	span.Finish()

	// now that we have a list of workflow types
	// iterate over them to extract their names
	list := make([]exec.WFWithStates, len(ts))
	for i, t := range ts {
		list[i].Name = t.WorkflowName.String()
		list[i].Display = t.Display
		list[i].Version = t.Version.String()
		list[i].Option = t.Option
		list[i].States = make([]exec.Named, len(t.Statenames))
		for j := range t.Statenames {
			list[i].States[j].Name = t.Statenames[j].Name
			list[i].States[j].Display = t.Statenames[j].Display
		}
		log.Println("schema:", BO, "wf type", syntax.VersionedName(t.WorkflowName, t.Version))
	}

	respondJSON(w, span0, list)
}

// List is the HandlerFunc for the /list operation
func (s Server) List(w http.ResponseWriter, r *http.Request) {
	span0 := opentracing.SpanFromContext(r.Context())

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	BOID := r.FormValue("boid")
	Rolenames := r.Form["rolename"] // list
	Option := r.FormValue("option")

	if BOID == "" {
		err = errors.New("must have boid and option (conforming to 'option-string' with attribute bo), optional rolename (multiple)")
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	o, err := option.Parse(Option)
	if err != nil {
		err = fmt.Errorf("illegal option string: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}
	BO := o.GetValue("bo")

	span0.SetTag("boid", BOID).SetTag("rolenames", Rolenames).SetTag("option", o.String()).SetTag("bo", BO)

	span := opentracing.StartSpan("registry", opentracing.ChildOf(span0.Context()))
	ts, err := reg.WorkflowsOf(r.Context(), s.TemporalClient, Option, Rolenames)
	if err != nil {
		err = fmt.Errorf("registry: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span.SetTag("error", err.Error()).Finish()
		return
	}
	span.SetTag("count", len(ts))
	span.Finish()

	// now that we have a list of workflow types
	// iterate over them and try to get their states:
	list := make([]exec.WFState, len(ts))
	for i, t := range ts {
		WorkflowID := syntax.BusinessWorkflowIDof(t.WorkflowName, BO, BOID)
		state, exists, err := stateOf(r.Context(), s.TemporalClient, WorkflowID, Rolenames, false) // false = no error if missing
		if err != nil {
			err = fmt.Errorf("list wfs: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			span0.SetTag("error", err.Error())
			return
		}
		if exists {
			list[i] = state
			log.Println("list:", WorkflowID, "living wf instance", syntax.VersionedName(state.Name, state.Version), "for wf type", syntax.VersionedName(t.WorkflowName, t.Version))
		} else {
			// a creatable wf has an empty current state
			list[i].Named = syntax.Named{
				Name:        t.WorkflowName,
				Description: t.Description,
				Display:     t.Display,
				Roles:       t.Roles,
				Option:      t.Option,
			}
			list[i].ID = WorkflowID
			list[i].Version = t.Version
			list[i].Idle = true
			list[i].CurrentAction = ""
			list[i].TaskQueue = t.TaskQueue
			log.Println("list:", WorkflowID, "creatable wf type", syntax.VersionedName(t.WorkflowName, t.Version))
		}
	}

	respondJSON(w, span0, list)
}

// Create is the HandlerFunc for the /create operation
func (s Server) Create(w http.ResponseWriter, r *http.Request) {
	span0 := opentracing.SpanFromContext(r.Context())

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	WorkflowID := syntax.BusinessWorkflowID(r.FormValue("id"))
	TaskQueue := r.FormValue("taskqueue")
	if WorkflowID == "" || TaskQueue == "" {
		err = errors.New("must have id(wf-name.bo.boid) and taskqueue")
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}
	WorkflowNameWithoutVersion, _, _, err := WorkflowID.Decompose()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	span0.LogEvent("query-registry")
	WorkflowName, err := reg.Versioned(r.Context(), s.TemporalClient, WorkflowNameWithoutVersion)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	span0.SetTag("wf-version", WorkflowName).SetTag("id", WorkflowID).SetTag("taskqueue", TaskQueue).LogEvent("ExecuteWorkflow")
	cm := contextualMap(r, span0)
	wfo := client.StartWorkflowOptions{
		ID:        string(WorkflowID),
		TaskQueue: TaskQueue,
		Memo:      toMemo(cm),
	}
	wr, err := s.TemporalClient.ExecuteWorkflow(r.Context(), wfo, WorkflowName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	respondJSON(w, span0, map[string]string{"id": wr.GetID(), "runid": wr.GetRunID()})
}

// Inspect is the HandlerFunc for the /inspect operation
func (s Server) Inspect(w http.ResponseWriter, r *http.Request) {
	s.inspect(w, r, false)
}

// InspectSchema is the HandlerFunc for the /inspectschema operation
func (s Server) InspectSchema(w http.ResponseWriter, r *http.Request) {
	s.inspect(w, r, true)
}

func (s Server) inspect(w http.ResponseWriter, r *http.Request, withSchema bool) {
	span0 := opentracing.SpanFromContext(r.Context())

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	WorkflowID := syntax.BusinessWorkflowID(r.FormValue("id"))
	Rolenames := r.Form["rolename"] // list

	if WorkflowID == "" {
		err = errors.New("must have id(wf-name.bo.boid), optional rolename (multiple)")
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	span0.SetTag("id", WorkflowID).SetTag("rolenames", Rolenames).SetTag("with-schema", withSchema)
	state, _, err := stateOf(r.Context(), s.TemporalClient, WorkflowID, Rolenames, true) // true = must exist
	if err != nil {
		err = fmt.Errorf("inspect wf: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	span0.SetTag("fse-state", state.CurrentState.Name)
	for i, tr := range state.Triggers {
		span0.SetTag(fmt.Sprintf("fse-trigger-%d", i+1), tr.Name)
	}

	if !withSchema {
		respondJSON(w, span0, state)
		return
	}

	schema, ok := s.syntaxOf(state)
	if !ok {
		err = fmt.Errorf("inspect wf schema %v.%v: not found", state.Name, state.Version)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	stateWithSchema := exec.WFStateWithSchema{
		WFState: state,
		Schema:  schema,
	}
	respondJSON(w, span0, stateWithSchema)
}

func (s Server) syntaxOf(state exec.WFState) (*syntax.Workflow, bool) {
	for _, wf := range s.Cache.Workflows() {
		if wf.Name == state.Name && wf.Version == state.Version {
			return wf, true
		}
	}
	return nil, false
}

// Signal is the HandlerFunc for the /signal operation
func (s Server) Signal(w http.ResponseWriter, r *http.Request) {
	span0 := opentracing.SpanFromContext(r.Context())

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	WorkflowID := syntax.BusinessWorkflowID(r.FormValue("id"))
	RunID := ""
	signal := r.FormValue("signal")

	if WorkflowID == "" || signal == "" {
		err = errors.New("must have id(wf-name.bo.boid) and signal")
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	span0.SetTag("id", WorkflowID).SetTag("signal", signal)
	cm := contextualMap(r, span0)
	cm[""] = signal
	err = s.TemporalClient.SignalWorkflow(r.Context(), string(WorkflowID), RunID, exec.SignalWorkflowSignalEvent, cm)
	if err != nil {
		err = fmt.Errorf("signal %q: %v", signal, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	span := opentracing.StartSpan("inspect", opentracing.ChildOf(span0.Context()))
	defer span.Finish()
	inner := opentracing.ContextWithSpan(r.Context(), span)
	s.Inspect(w, r.WithContext(inner))
}

// History implements the /history operation
func (s Server) History(w http.ResponseWriter, r *http.Request) {
	span0 := opentracing.SpanFromContext(r.Context())

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	WorkflowID := syntax.BusinessWorkflowID(r.FormValue("id"))
	Rolenames := r.Form["rolename"] // list

	if WorkflowID == "" {
		err = errors.New("must have id(wf-name.bo.boid), optional rolename (multiple)")
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	span0.SetTag("id", WorkflowID).SetTag("rolenames", Rolenames)

	hist, err := getWorkflowHistory(r.Context(), s.TemporalClient, string(WorkflowID), "")
	if err != nil {
		err = fmt.Errorf("getWorkflowHistory: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	respondJSON(w, span0, hist.Events)
}

func getWorkflowHistory(ctx context.Context, client client.Client, id, runID string) (*history.History, error) {
	var hist history.History
	iter := client.GetWorkflowHistory(ctx, id, runID, false, enumsV1.HISTORY_EVENT_FILTER_TYPE_ALL_EVENT)
	for iter.HasNext() {
		event, err := iter.Next()
		if err != nil {
			return nil, err
		}
		hist.Events = append(hist.Events, event)
	}
	return &hist, nil
}

// Terminate is the HandlerFunc for the /terminate operation
func (s Server) Terminate(w http.ResponseWriter, r *http.Request) {
	span0 := opentracing.SpanFromContext(r.Context())

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	WorkflowID := syntax.BusinessWorkflowID(r.FormValue("id"))
	RunID := ""
	Reason := r.FormValue("reason")
	if Reason == "" {
		Reason = "terminated by user request"
	}

	if WorkflowID == "" {
		err = errors.New("must have id(wf-name.bo.boid), optional reason")
		http.Error(w, err.Error(), http.StatusBadRequest)
		span0.SetTag("error", err.Error())
		return
	}

	span0.SetTag("id", WorkflowID).SetTag("reason", Reason)
	err = s.TemporalClient.TerminateWorkflow(r.Context(), string(WorkflowID), RunID, Reason)
	if err != nil {
		err = fmt.Errorf("terminate %v: %v", WorkflowID, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		span0.SetTag("error", err.Error())
		return
	}

	return // nothing to report besides status OK
}

/*
const (
	WORKFLOW_EXECUTION_STATUS_UNSPECIFIED WorkflowExecutionStatus = 0
	// Value 1 is hardcoded in SQL persistence.
	WORKFLOW_EXECUTION_STATUS_RUNNING          WorkflowExecutionStatus = 1
	WORKFLOW_EXECUTION_STATUS_COMPLETED        WorkflowExecutionStatus = 2
	WORKFLOW_EXECUTION_STATUS_FAILED           WorkflowExecutionStatus = 3
	WORKFLOW_EXECUTION_STATUS_CANCELED         WorkflowExecutionStatus = 4
	WORKFLOW_EXECUTION_STATUS_TERMINATED       WorkflowExecutionStatus = 5
	WORKFLOW_EXECUTION_STATUS_CONTINUED_AS_NEW WorkflowExecutionStatus = 6
	WORKFLOW_EXECUTION_STATUS_TIMED_OUT        WorkflowExecutionStatus = 7
)
*/

// stateOf queries a particular wf instance. If it exists it returns its state.
// If it doesn't exist it returns false, if mustExist is true also an error. If there is an error, it returns it.
func stateOf(ctx context.Context, cli client.Client, WorkflowID syntax.BusinessWorkflowID, roles []string, mustExist bool) (exec.WFState, bool, error) {
	span0 := opentracing.SpanFromContext(ctx)
	span := span0.Tracer().StartSpan("stateOf", opentracing.ChildOf(span0.Context()))
	defer span.Finish()
	span.SetTag("id", WorkflowID)

	var state exec.WFState
	EmptyRunID := "" // it can be empty

	we, err := cli.DescribeWorkflowExecution(ctx, string(WorkflowID), EmptyRunID) // TODO: this call provides probably more noteable properties for the inspect/list api
	if err != nil {
		if mustExist {
			span.SetTag("error", err.Error())
			return state, false, fmt.Errorf("describe wf '%v': %v", WorkflowID, err)
		}
		if err.Error() == sql.ErrNoRows.Error() {
			// returned by temporal.io with sql persistence (`sql: no rows in result set`)
			return state, false, nil
		}
		if strings.HasPrefix(err.Error(), "Workflow execution not found.") {
			// returned by temporal.io with cassandra persistence
			return state, false, nil
		}
		return state, false, fmt.Errorf("describe wf '%v' (while must not exist): %v", WorkflowID, err)
	}
	wfi := we.GetWorkflowExecutionInfo()
	span.SetTag("wf state", wfi.Status.String())
	switch wfi.Status {
	case enumsV1.WORKFLOW_EXECUTION_STATUS_RUNNING, enumsV1.WORKFLOW_EXECUTION_STATUS_CONTINUED_AS_NEW:
		// OK, I exist, continue
	default:
		// any other state is as if I don't exist
		if mustExist {
			err = fmt.Errorf("wf '%v' not/no more open", WorkflowID)
			span.SetTag("error", err.Error())
			return state, false, err
		}
		return state, false, nil
	}

	span.SetTag("runID", EmptyRunID).LogEvent("QueryWorkflow")
	val, err := cli.QueryWorkflow(ctx, string(WorkflowID), EmptyRunID, exec.QueryWorkflowWFStateFor, roles)
	if err != nil {
		span.SetTag("error", err.Error())
		return state, true, err
	}
	if !val.HasValue() {
		err = fmt.Errorf("query wf '%v' get value: has no value", WorkflowID)
		span.SetTag("error", err.Error())
		return state, true, err
	}
	span.LogEvent("GetStateValue")
	err = val.Get(&state)
	if err != nil {
		err = fmt.Errorf("query wf '%v' get value: %v", WorkflowID, err)
		span.SetTag("error", err.Error())
		return state, true, err
	}

	return state, true, nil
}

// Definition is the HandlerFunc for /. It provides a simple UI for listing and
// showing workflow definitions hosted an this Server.
func (s Server) Definition(w http.ResponseWriter, r *http.Request) {
	name := syntax.Unversioned(r.URL.Query().Get("name"))
	version := syntax.Unversioned(r.URL.Query().Get("version"))
	isEmbed := (r.URL.Query().Get("embed") != "")

	workflows := s.Cache.Workflows()

	switch {
	case name == "":
		tn := "list"
		if isEmbed {
			tn = "embed"
		}
		err := tpl.TemplateList().ExecuteTemplate(w, tn, workflows)
		if err != nil {
			log.Println(err)
		}
		return

	default:
		tn := "item"
		if isEmbed {
			tn = "embed"
		}
		for _, wf := range workflows {
			if wf.Name == name && (version == "" || wf.Version == version) {
				err := tpl.TemplateItem(wf).ExecuteTemplate(w, tn, wf)
				if err != nil {
					log.Println(err)
				}
				return
			}
		}
	}
	http.Error(w, "not found", http.StatusNotFound)
}
