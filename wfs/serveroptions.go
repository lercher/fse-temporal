package wfs

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/lercher/fse-temporal/cache"
	"gitlab.com/lercher/fse-temporal/reg"
	"gitlab.com/lercher/fse-temporal/trace"
	"gitlab.com/lercher/fse-temporal/watcher"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/workflow"
)

// ServerOptions contains the fields to configure the workflow server
type ServerOptions struct {
	WorkflowsGlobPattern     string        // WorkflowsGlobPattern ist the glob pattern for *.fse files this Server loads and hosts
	WatcherDebounce          time.Duration // WatcherDebounce is the debounce time for a file system watcher if positive. Else no watcher is attached.
	ResetRegistry            bool          // ResetRegistry sholod be set to true, if the registry workflow needs to be restarted
	TaskqueueName            string        // TaskqueueName is the name of the temporal task queue this host listens on, it's the same for FSE workflows, the registry wf and for actions
	OpenTracingServicename   string        // OpenTracingServicename is the name this server publishes its spans under
	workflow.ActivityOptions               // ActivityOptions besides TaskQueue=ActionqueueName() are passed to all activities started by the ActivityDispatcher
}

// InitializeServer initializes a new Server based on the ServerOptions.
// add can be nil. If not it is called when the framework decides to start a new temporal worker
// b/c the FSE definitions need to be replaced. It should be used to add custom activities
// and probably custom workflows to the worker.
func (so ServerOptions) InitializeServer(cli client.Client, add MoreActivities) (Server, error) {
	s := Server{Add: add}

	// tracer
	tr, tracecloser, err := trace.Initialize(so.OpenTracingServicename, log.New(os.Stderr, "TRACE", log.LstdFlags))
	if err != nil {
		return s, fmt.Errorf("opentracing: %v", err)
	}
	s.Tracer = tr
	s.tracecloser = tracecloser

	// cache
	s.Cache = cache.New()
	workflows, err := globWorkflows(s.Cache, so.WorkflowsGlobPattern)
	if err != nil {
		return s, fmt.Errorf("glob %v: %v", so.WorkflowsGlobPattern, err)
	}
	if len(workflows) == 0 {
		return s, fmt.Errorf("no workflows found at %v", so.WorkflowsGlobPattern)
	}

	// temporal client
	s.TemporalClient = cli

	// registry worker
	if so.ResetRegistry {
		log.Println("resetting global registry workflow ...")
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		err := cli.TerminateWorkflow(ctx, string(reg.WorkflowID), "", "registry reset requested by wfs -reset")
		if err != nil {
			dl, _ := ctx.Deadline()
			log.Printf("ERR terminate global registry WF %q (deadline %v): %v", reg.WorkflowID, dl, err)
		} else {
			log.Println("global registry workflow reset sucessfull (by terminating it)")
		}
		cancel()
	}

	// fse workflow workers
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	s.fseWorker, s.regWorker, err = startWorkersFseReg(ctx, s.TemporalClient, so.TaskqueueName, so.ActivityOptions, s.Cache, workflows, s.Add)
	if err != nil {
		return s, fmt.Errorf("fse and registry workflows worker start: %v", err)
	}

	// watcher
	var watch *watcher.Watcher
	if so.WatcherDebounce > 0 {
		watch, err = watcher.New(so.WorkflowsGlobPattern, so.WatcherDebounce)
		if err != nil {
			return s, fmt.Errorf("watch %v: %v", so.WorkflowsGlobPattern, err)
		}
		s.Watcher = watch
		go s.watch(so)
	}

	return s, nil
}
