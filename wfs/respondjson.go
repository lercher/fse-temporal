package wfs

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/opentracing/opentracing-go"
)

func respondJSON(w http.ResponseWriter, span opentracing.Span, obj interface{}) {
	span.LogEvent("respondJSON")
	enc := json.NewEncoder(w)
	enc.SetIndent("", "  ")
	err := enc.Encode(obj)
	if err != nil {
		s := fmt.Sprintln("ERROR", "serializing to JSON %T", obj)
		// ignore further errors, b/c the client is already in error
		_, _ = w.Write([]byte("\n\n"))
		_, _ = w.Write([]byte(s))
	}
}
