package exec

type result struct {
	terminate bool
	halt      bool
	gs        gotoState
}

func (r result) escalate() bool {
	return r.terminate || !r.gs.isZero()
}
