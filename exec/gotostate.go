package exec

import (
	"fmt"

	"gitlab.com/lercher/fse-temporal/syntax"
)

type gotoState struct {
	stateName  syntax.Unversioned
	envVarName string
}

func (gs gotoState) isZero() bool {
	return gs.stateName == ""
}

func (gs gotoState) String() string {
	if gs.envVarName != "" {
		return fmt.Sprintf("[%v by %v]", gs.stateName, gs.envVarName)
	}
	return fmt.Sprintf("%q", gs.stateName)
}

func literalGotoState(sn string) gotoState {
	return gotoState{
		stateName: syntax.Unversioned(sn),
	}
}

func indirectGotoState(envVarName string, environment map[string]string) gotoState {
	val := environment[envVarName]
	if val == EnvironmentValueNew {
		// *new* is not a valid state name
		val = "" // silently no goto
	}
	return gotoState{
		stateName:  syntax.Unversioned(val),
		envVarName: envVarName,
	}
}
