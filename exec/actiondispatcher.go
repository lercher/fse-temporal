package exec

import (
	"go.temporal.io/sdk/workflow"
)

// ActionDispatcher dispatches all named calls to workflow.ExecuteActivity
func ActionDispatcher(in *Interpreter, name string, contextualMapAsJSON string, parameters []string) workflow.Future {
	ctx := workflow.WithActivityOptions(in.Ctx, in.ActivityOptions)
	logger := workflow.GetLogger(ctx)
	logger.Info("funcall", "contextualMap", contextualMapAsJSON, "name", name, "parameters", parameters)

	return workflow.ExecuteActivity(ctx, name, contextualMapAsJSON, parameters)
}
