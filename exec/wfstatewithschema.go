package exec

import (
	"gitlab.com/lercher/fse-temporal/syntax"
)

// WFStateWithSchema describes the current fse workflow instance including its Schema
type WFStateWithSchema struct {
	WFState
	Schema *syntax.Workflow
}
