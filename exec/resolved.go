package exec

import (
	"fmt"

	"go.temporal.io/sdk/workflow"
)

// Resolved implements a resolved workflow.Future of type string
type Resolved string

// Get puts r into valuePtr unless valuePtr is nil
func (r Resolved) Get(ctx workflow.Context, valuePtr interface{}) error {
	if valuePtr == nil {
		return nil
	}
	if ps, ok := valuePtr.(*string); ok {
		*ps = string(r)
		return nil
	}
	return fmt.Errorf("valuePtr of Resolved.Get must be nil or *string, got %T", valuePtr)
}

// IsReady returns true
func (r Resolved) IsReady() bool {
	return true
}
