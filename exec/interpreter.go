package exec

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/lercher/fse-temporal/syntax"

	"go.temporal.io/sdk/log"
	"go.temporal.io/sdk/workflow"
)

// Actions is a dispatcher for handling funcalls by name defined as a consequence in an FSE.
type Actions func(in *Interpreter, name string, contextualMapAsJson string, parameters []string) workflow.Future

// Interpreter is an execution environment for an FSE for termporal.io
type Interpreter struct {
	WF                *syntax.Workflow
	Ctx               workflow.Context
	Actions           Actions
	Environment       map[string]string
	ActivityOptions   workflow.ActivityOptions
	CurrentState      *syntax.State
	idle              bool
	busywith          string
	living            bool
	ended             time.Time
	Timer             workflow.Future
	TimerConsequences []syntax.Consequence
	TimerDeadline     time.Time
	TimerDuration     time.Duration
	logger            log.Logger
}

const (
	// EnvironmentPrevious is the Interpreter's Environment key for the previous state the wf was idle in, $PREVIOUS
	EnvironmentPrevious = `$PREVIOUS`

	// EnvironmentPreviousDisplay is the Interpreter's Environment key for the previous state's display name the wf was idle in, $PREVIOUS
	EnvironmentPreviousDisplay = `$PREVIOUSDISPLAY`

	// EnvironmentEvent is the Interpreter's Environment key for the current signal that is processed, $EVENT
	EnvironmentEvent = `$EVENT`

	// EnvironmentEventDisplay is the Interpreter's Environment key for the current signal's display name that is processed, $EVENT
	EnvironmentEventDisplay = `$EVENTDISPLAY`

	// EnvironmentCurrent is the Interpreter's Environment key for the current state the wf has entered, $CURRENT
	EnvironmentCurrent = `$CURRENT`

	// EnvironmentCurrentDisplay is the Interpreter's Environment key for the current state's display name the wf has entered, $CURRENT
	EnvironmentCurrentDisplay = `$CURRENTDISPLAY`

	// EnvironmentEventTimer is the value bound to $EVENT after a timer event was triggered
	EnvironmentEventTimer = "*timer*"

	// EnvironmentValueNew is the initial value of EnvironmentPrevious/`$PREVIOUS`
	// and its value "*new*" may appear in some logs representing the non-existant
	// state before starting the workflow.
	EnvironmentValueNew = `*new*`
)

// New creates a new interpreter with an environment for variables for an FSE.
// If actions is nil, all funcalls are dispatched to tempoaral.io actions
// that must be either string->string or []string->string according
// to the parameter list's length of 0 or more accordingly. The first parameter is
// always a JSON serialized string containing the contextualMap paramters of the create
// or signal operation.
func New(ctx workflow.Context, wf *syntax.Workflow, actions Actions) *Interpreter {
	if actions == nil {
		actions = ActionDispatcher
	}
	return &Interpreter{
		WF:          wf,
		Ctx:         ctx,
		Actions:     actions,
		Environment: make(map[string]string),
		ActivityOptions: workflow.ActivityOptions{
			ScheduleToStartTimeout: time.Hour * 8,
			StartToCloseTimeout:    time.Hour * 24,
		},
		logger: workflow.GetLogger(ctx),
	}
}

func (in *Interpreter) describeState() (WFState, error) {
	return Describe(in, true, nil)
}

func (in *Interpreter) describeStateFor(roles []string) (WFState, error) {
	return Describe(in, false, roles)
}

// Run evaluates an FSE within a temporal.io workflow function.
// Typical usage is: parse an FSE source, create an Interpreter with an Actions dispatcher func.
// The initialContextualMap is either nil or initialized by the memo passed to workflow instance creation.
func (in *Interpreter) Run(initialContextualMap map[string]string) error {
	if in == nil || in.WF == nil || len(in.WF.States) == 0 {
		// without a WF nor top level states, we are finished immediately
		return nil
	}

	// attach queries:
	workflow.SetQueryHandler(in.Ctx, QueryWorkflowWFState, in.describeState)
	workflow.SetQueryHandler(in.Ctx, QueryWorkflowWFStateFor, in.describeStateFor)

	in.Environment[EnvironmentPrevious] = EnvironmentValueNew
	in.Environment[EnvironmentPreviousDisplay] = ""

	// initialize the looped over contextual map by the one extracted from the creation memo
	var initialContextualMapAsJSON string
	if bytes, err := json.Marshal(initialContextualMap); err == nil {
		initialContextualMapAsJSON = string(bytes)
	}

	return in.eventLoop(in.WF.States[0].Name, initialContextualMapAsJSON)
}

func (in *Interpreter) eventLoop(initial syntax.Unversioned, initialContextualMapAsJSON string) error {
	var err error
	detectLoopCount := 0
	contextualMapAsJSON := initialContextualMapAsJSON
	res := result{gs: gotoState{stateName: initial}} // initialize the initial state of the complete workflow

	in.living = true
	for {
		switch {
		case res.terminate:
			in.idle = true
			in.living = false
			in.ended = workflow.Now(in.Ctx)
			return nil // Workflow done successfully

		case !res.gs.isZero():
			detectLoopCount++
			newstate := res.gs
			if s, ok := in.WF.AllStates[newstate.stateName]; ok {
				res, err = in.gotostate(s, contextualMapAsJSON)
				if err != nil {
					return fmt.Errorf("initializing %v: %v", newstate, err)
				}
				if detectLoopCount > 20 {
					return fmt.Errorf("initializing %v: state-hop count is too large (%v), check the goto consequences", newstate, detectLoopCount)
				}
				continue
			} else {
				// state unknown
				// - this wont't happen for a plain goto b/c the parser keeps this safe
				// - it may happen on a "goto $v" if the value of $x is not a known state
				return fmt.Errorf("goto %v failed: target state unknown", newstate)
			}
		}
		detectLoopCount = 0 // not a tight loop with goto state consequences

		// wf idles, it should wait for a Timer or for an event's trigger, therefor
		// compose a selector waiting for "trigger" or the timer firing
		var evt syntax.Unversioned
		var consequences []syntax.Consequence
		s := workflow.NewSelector(in.Ctx)
		triggerChannel := workflow.GetSignalChannel(in.Ctx, SignalWorkflowSignalEvent)
		s.AddReceive(triggerChannel, func(c workflow.ReceiveChannel, more bool) {
			var contextualMap map[string]string
			c.Receive(in.Ctx, &contextualMap)
			evt = syntax.Unversioned(contextualMap[""])
			delete(contextualMap, "")
			contextualMapAsJSON = ""
			if bytes, err := json.Marshal(contextualMap); err == nil {
				contextualMapAsJSON = string(bytes)
			}
			if tr, ok := in.trigger(evt); ok {
				in.logger.Info("triggered", "event", evt)
				in.Environment[EnvironmentEvent] = string(evt)
				in.Environment[EnvironmentEventDisplay] = tr.Display
				consequences = tr.Consequences
			} else {
				in.logger.Warn("received non-active trigger", "event", evt)
			}
		})
		if in.Timer != nil {
			s.AddFuture(in.Timer, func(f workflow.Future) {
				contextualMapAsJSON = ""
				evt = EnvironmentEventTimer
				in.Environment[EnvironmentEvent] = string(evt)
				in.Environment[EnvironmentEventDisplay] = fmt.Sprint("after ", in.TimerDuration)
				in.logger.Info("timer")
				consequences = in.TimerConsequences
				contextualMapAsJSON = initialContextualMapAsJSON
				in.Timer = nil
				in.TimerConsequences = nil
				in.TimerDeadline = time.Time{}
				in.TimerDuration = 0
			})
		}
		in.idle = true
		s.Select(in.Ctx) // waits for a signal to be received (s.AddReceive) or a timer event (s.AddFuture) declared above
		in.idle = false
		in.Environment[EnvironmentPrevious] = in.Environment[EnvironmentCurrent]
		in.Environment[EnvironmentPreviousDisplay] = in.Environment[EnvironmentCurrentDisplay]
		res, err = in.doAll(consequences, contextualMapAsJSON)
		if err != nil {
			return fmt.Errorf("event %q: %v", evt, err)
		}
		// continue for-ever, hand over the new res to start of for loop
	}
}

// currentTriggers adds all events of the current state and all its parent states to a list
func (in *Interpreter) currentTriggers() (list []*syntax.Event) {
	if !in.idle {
		// the current state is not relevant, if the WF is not idle
		// i.e. if it's working on an action, there are no triggers
		return nil
	}
	// loop over in.CurrentState up the Parent chain
	for s := in.CurrentState; s != nil; s = s.Parent {
		for i := range s.Events {
			switch s.Events[i].Type {
			case "final-event": // not inherited
				// only relevant if the currently inspected state is the interpreter's current one
				if s == in.CurrentState {
					list = append(list, &s.Events[i])
				}
			case "event": // inherited
				// all events in the parent chain are relevant
				list = append(list, &s.Events[i])
			}
		}
	}
	return list
}

// trigger finds a named event within currentTriggers
func (in *Interpreter) trigger(name syntax.Unversioned) (*syntax.Event, bool) {
	triggers := in.currentTriggers()
	for i := range triggers {
		if triggers[i].Name == name {
			return triggers[i], true
		}
	}
	return nil, false
}

func (in *Interpreter) gotostate(s *syntax.State, contextualMapAsJSON string) (result, error) {
	var res result
	if in.CurrentState == s {
		return res, nil
	}

	// we don't change EnvironmentPrevious* here intentionally,
	// b/c we want to preserve the value before an event/signal
	// even if we do hops over multiple states.
	in.Environment[EnvironmentCurrent] = string(s.Name)
	in.Environment[EnvironmentCurrentDisplay] = s.DisplayOrName()

	var err error

	leaving := syntax.Unversioned(EnvironmentValueNew)
	if in.CurrentState != nil {
		leaving = in.CurrentState.Name
	}
	in.CurrentState = s

	in.logger.Info("initializing",
		"state", s.Name,
		"leaving", leaving,
	)
	in.Timer = nil
	in.TimerConsequences = nil
	in.TimerDeadline = time.Time{}
	in.TimerDuration = 0
	for i := range s.Events {
		switch s.Events[i].Type {
		case "initialize":
			res, err = in.doAll(s.Events[i].Consequences, contextualMapAsJSON)
			if err != nil {
				return res, fmt.Errorf("%s of %s: %v", s.Events[i].Type, s.Name, err)
			}

		case "after":
			after, err := time.ParseDuration(s.Events[i].Parameter)
			if err != nil {
				return res, fmt.Errorf("%s of %s: %v", s.Events[i].Type, s.Name, err)
			}
			in.logger.Info("after", "delay", after)
			in.Timer = workflow.NewTimer(in.Ctx, after)
			in.TimerConsequences = s.Events[i].Consequences
			in.TimerDeadline = workflow.Now(in.Ctx).Add(after)
			in.TimerDuration = after
		}
	}
	return res, nil
}

func (in *Interpreter) doAll(cs []syntax.Consequence, contextualMapAsJSON string) (result, error) {
	for i := range cs {
		res, err := in.doOne(cs[i], contextualMapAsJSON)
		switch {
		case err != nil:
			return res, fmt.Errorf("action #%d: %v", i, err)
		case res.halt:
			res.halt = false
			break
		case res.escalate():
			return res, nil
		}
	}
	return result{}, nil
}

func (in *Interpreter) setEnvFrom(f workflow.Future, c syntax.Consequence) error {
	var s string
	err := f.Get(in.Ctx, &s)
	if err != nil {
		return fmt.Errorf("string result $assign = funcall(): %v", err)
	}
	in.logger.Info("action setenv by assign", "key", c.LVar, "value", s)
	in.Environment[c.LVar] = s
	return nil
}

func (in *Interpreter) setEnvFromMap(f workflow.Future, _ syntax.Consequence) error {
	var m map[string]string
	err := f.Get(in.Ctx, &m)
	if err != nil {
		return fmt.Errorf("map result $ = funcall(): %v", err)
	}

	for k, v := range m {
		if strings.HasPrefix(k, "$") {
			in.Environment[k] = v
			in.logger.Info("action setenv by map", "key", k, "value", v)
		} else {
			in.logger.Warn("returned map funcall() value needs to have a $-prefixed key: %q=%q", k, v)
		}
	}
	return nil
}

func (in *Interpreter) doOne(c syntax.Consequence, contextualMapAsJSON string) (res result, err error) {
	j, _ := json.Marshal(c.Action)
	in.logger.Info(c.Action, "details", string(j))

	switch c.Action {
	default:
		return res, fmt.Errorf("unknown action type %q", c.Action)

	case "assign":
		defer func() {
			in.busywith = ""
		}()
		future, err := in.eval(c, contextualMapAsJSON)
		if err != nil {
			return res, err
		}
		switch c.LVar {
		default:
			// $envvar = $other
			// $envvar = funcall(x x x)
			return res, in.setEnvFrom(future, c)

		case "$$":
			// $$ = funcall(x x x)
			return res, in.setEnvFromMap(future, c)
		}

	case "funcall":
		// funcall(x x x)
		defer func() {
			in.busywith = ""
		}()
		future, err := in.eval(c, contextualMapAsJSON)
		if err != nil {
			return res, err
		}
		return res, future.Get(in.Ctx, nil)

	case "if-goto", "goto":
		err = in.doIf(c, func() {
			switch {
			case c.Goto == "":
				res.halt = true
			case c.Goto[0] == '$':
				res.gs = indirectGotoState(c.Goto, in.Environment)
			default:
				res.gs = literalGotoState(c.Goto)
			}
		})
		return res, err

	case "if-halt", "halt":
		err = in.doIf(c, func() {
			res.halt = true
		})
		return res, err

	case "terminate":
		res.terminate = true
		return res, nil
	}
}

func (in *Interpreter) doIf(c syntax.Consequence, iftrue func()) error {
	if c.LVar == "" {
		// unconditional goto/halt
		iftrue()
		return nil
	}

	lvalue := in.Environment[c.LVar]
	evaluatesToTrue := false
	switch c.Operator {
	case "":
		// if $var goto/halt
		evaluatesToTrue = (lvalue != "")
	case "==":
		// if $var == x goto/halt
		if len(c.Parameters) != 1 {
			return fmt.Errorf("illegal number of parameters for %q: is %d, should be 1", c.Operator, len(c.Parameters))
		}
		rvalue := in.rvalue(c.Parameters[0].Text, c.Parameters[0].Var)
		evaluatesToTrue = (lvalue == rvalue)
	case "!=":
		// if $var != x goto/halt
		if len(c.Parameters) != 1 {
			return fmt.Errorf("illegal number of parameters for %q: is %d, should be 1", c.Operator, len(c.Parameters))
		}
		rvalue := in.rvalue(c.Parameters[0].Text, c.Parameters[0].Var)
		evaluatesToTrue = (lvalue != rvalue)
	default:
		return fmt.Errorf("unsupported comparison op %q", c.Operator)
	}

	if evaluatesToTrue {
		// if condition evaluated to true
		iftrue()
	}
	return nil
}

func (in *Interpreter) rvalue(literal, variable string) string {
	if variable != "" {
		return in.Environment[variable]
	}
	return literal
}

func (in *Interpreter) rvalues(ps []syntax.Parameter) (list []string) {
	list = make([]string, len(ps))
	for i := range ps {
		list[i] = in.rvalue(ps[i].Text, ps[i].Var)
	}
	return list
}

func (in *Interpreter) eval(c syntax.Consequence, contextualMapAsJSON string) (workflow.Future, error) {
	switch c.Function {
	case "":
		if len(c.Parameters) != 1 {
			return nil, fmt.Errorf("illegal number of parameters for assignment: is %d, shoud be 1", len(c.Parameters))
		}
		rvalue := in.rvalue(c.Parameters[0].Text, c.Parameters[0].Var)
		return Resolved(rvalue), nil
	default:
		in.busywith = c.Function
		future := in.Actions(in, c.Function, contextualMapAsJSON, in.rvalues(c.Parameters))
		return future, nil
	}
}
