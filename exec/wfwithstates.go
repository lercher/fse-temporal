package exec

// WFWithStates records an FSE workflow type's name and a list of state names
type WFWithStates struct {
	Named
	Version string  `json:"version"` // Version is the workflow's version string
	Option  string  `json:"option"`  // Option is an option string, that must be parseable via option.Parse
	States  []Named `json:"states"`  // States is the list of states of this workflow
}

// Named pairs a key with a display text
type Named struct {
	Name    string `json:"name"`    // Name is the key of the workflow type or state
	Display string `json:"display"` // Display is a text intended to be shown in a GUI
}
