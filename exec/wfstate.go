package exec

import (
	"time"

	"gitlab.com/lercher/fse-temporal/syntax"

	"go.temporal.io/sdk/workflow"
)

// WFState describes the current fse workflow instance
type WFState struct {
	syntax.Named
	Living        bool                      `json:"living"`
	Idle          bool                      `json:"idle"`
	CurrentAction string                    `json:"currentAction"`
	TaskQueue     string                    `json:"taskqueue"`
	ID            syntax.BusinessWorkflowID `json:"id"`
	Version       syntax.Unversioned        `json:"version"`
	RunID         string                    `json:"runid"`
	CurrentState  syntax.Named              `json:"currentState"`
	Triggers      []syntax.Named            `json:"triggers"`
	TimerDeadline time.Time                 `json:"timerDeadline"`
	Ended         time.Time                 `json:"ended"`
	Environment   map[string]string         `json:"environment"`
}

// Describe describes the workflow instance in its current state.
// If allTriggers is true, all current triggers are returned and roles is ignored.
// If allTriggers is false only those current triggers are returned that have matching roles in their "for" list.
func Describe(in *Interpreter, allTriggers bool, roles []string) (WFState, error) {
	temporalInfo := workflow.GetInfo(in.Ctx)
	fse := WFState{
		Named:         in.WF.Named,
		Version:       in.WF.Version,
		ID:            syntax.BusinessWorkflowID(temporalInfo.WorkflowExecution.ID),
		RunID:         temporalInfo.WorkflowExecution.RunID,
		TaskQueue:     temporalInfo.TaskQueueName,
		CurrentState:  syntax.Named{},
		Triggers:      nil,
		TimerDeadline: in.TimerDeadline,
		Environment:   in.Environment,
		Idle:          in.idle,
		CurrentAction: in.busywith,
		Living:        in.living,
		Ended:         in.ended,
	}

	triggers := in.currentTriggers()
	for i := range triggers {
		if allTriggers || triggers[i].IntersectsWith(roles) {
			fse.Triggers = append(fse.Triggers, triggers[i].Named)
		}
	}

	if in.CurrentState != nil {
		fse.CurrentState = in.CurrentState.Named
	}

	// logger := workflow.GetLogger(in.Ctx)
	// logger.Info("describe called", "allTriggers", allTriggers, "roles", roles, "fse", fse)

	return fse, nil
}

// Env is a convenience function for html/template to replace
// ((index .Current.Environment "$var")) by ((.Current.Env "$var"))
func (wfs WFState) Env(variable string) string {
	return wfs.Environment[variable]
}
