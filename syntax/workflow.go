package syntax

// Workflow is the parsed SemanticTree
type Workflow struct {
	Named
	Version    Unversioned            `json:"version,omitempty"` // Version is optional, but if you have one and you are parsing with a non empty path, it is an error to not include the version in the filename
	States     []State                `json:"states"`            // States are only the daclared top-level states
	Statenames []Statename            `json:"statenames"`        // Statenames lists all states names in text order
	AllStates  map[Unversioned]*State `json:"-"`                 // AllStates are all states and their substates indexed by Name
	Path       string                 `json:"-"`                 // Path is the filename, if the wf was parsed from an os.File
}

// registerState adds a State to AllStates and to Statenames
func (wf *Workflow) registerState(s *State) {
	wf.AllStates[s.Name] = s
	wf.Statenames = append(wf.Statenames, Statename{Name: s.Name, Display: s.Display})
}
