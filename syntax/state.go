package syntax

import (
	"sort"
)

// State is a workflow state
type State struct {
	Named
	Line   int     `json:"line"`   // Line is the linenumber within the source in which the "state" directive was declared
	Events []Event `json:"events"` // Events is the list of Events this State owns directly including initialization and delay events
	States []State `json:"states"` // States is the list of sub-states this state has
	Parent *State  `json:"-"`      // Parent is the state this state is a sub-state of
}

// Predecessors lists all states and events that can possibly enter this State.
// Inherited events are however only listed within their defining state.
// Predecessors are sorted by state-name.
func (s State) Predecessors(wf *Workflow) []Predecessor {
	statename := string(s.Name)
	var list []Predecessor
	for _, potentialSource := range wf.AllStates {
		p := Predecessor{Source: potentialSource}
		for _, e := range potentialSource.Events {
			addevent := false
			for _, c := range e.Consequences {
				if c.Goto == statename {
					addevent = true
				}
			}
			if addevent {
				p.Events = append(p.Events, e)
			}
		}
		if len(p.Events) > 0 {
			list = append(list, p)
		}
	}
	sort.Slice(list, func(i int, j int) bool { return list[i].Source.Name < list[j].Source.Name })
	return list
}
