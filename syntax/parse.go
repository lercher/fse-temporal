package syntax

import (
	"fmt"
	"io"
	"io/ioutil"

	"gitlab.com/lercher/fse-temporal/listener"
	"gitlab.com/lercher/fse-temporal/parser"

	"github.com/antlr4-go/antlr/v4"
)

// Parse parses FSE DSL syntax and returns a semantic tree and diagnostic information.
// The path is stored as Path in the resulting wf and within diagnostics.
func Parse(r io.Reader, path string) (*Workflow, []listener.Diagnostic, error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		if path != "" {
			return nil, nil, fmt.Errorf("%v: %v", path, err)
		}
		return nil, nil, err
	}

	input := antlr.NewInputStream(string(b)) // or NewFileStream
	lexer := parser.NewFSELexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := parser.NewFSEParser(stream)
	p.Interpreter.SetPredictionMode(antlr.PredictionModeSLL)

	// p has a default error listener that prints unwanted diagnostics, so we remove it first:
	el := listener.New(path)
	p.RemoveErrorListeners()
	p.AddErrorListener(el)
	p.BuildParseTrees = true

	// now parse the FSE source reader
	tree := p.Main()
	if el.ErrorCount() > 0 {
		if path != "" {
			return nil, el.Diagnostics, fmt.Errorf("%v: %d syntax error(s) detected", path, el.ErrorCount())
		}
		return nil, el.Diagnostics, fmt.Errorf("%d syntax error(s) detected", el.ErrorCount())
	}

	// create the AST by a walk of the parse tree
	w := newWlk(el, p.GetTokenStream())
	antlr.ParseTreeWalkerDefault.Walk(w, tree)
	w.check()
	if el.ErrorCount() > 0 {
		if path != "" {
			return nil, el.Diagnostics, fmt.Errorf("%v: %d semantic error(s) detected", path, el.ErrorCount())
		}
		return nil, el.Diagnostics, fmt.Errorf("%d semantic error(s) detected", el.ErrorCount())
	}

	w.wf.Path = path
	return &w.wf, nil, nil
}
