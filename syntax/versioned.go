package syntax

import (
	"fmt"
	"strings"
)

// Versioned is a name with an optional version
type Versioned string

func (v Versioned) String() string {
	return string(v)
}

// Unversioned is a name or a version, but not a combination name@version
type Unversioned string

func (uv Unversioned) String() string {
	return string(uv)
}

// VersionedName returns Name@Version if there is one, otherwise Name
func (wf Workflow) VersionedName() Versioned {
	return VersionedName(wf.Name, wf.Version)
}

// VersionedName returns Name@Version if there is one, otherwise Name
func VersionedName(name, version Unversioned) Versioned {
	if version == "" {
		return Versioned(name)
	}
	return Versioned(name + "@" + version)
}

// BusinessWorkflowID is a triple Unnamed.BO.BOID
type BusinessWorkflowID string

// BusinessWorkflowIDof creates a BusinessWorkflowID (wfname.bo.boid) from its components
func BusinessWorkflowIDof(workflowName Unversioned, bo, boID string) BusinessWorkflowID {
	return BusinessWorkflowID(string(workflowName) + "." + bo + "." + boID)
}

// Decompose splits a BusinessWorkflowID into its components (wfname.bo.boid) it was created from. The wfname part must not be empty.
func (id BusinessWorkflowID) Decompose() (workflowName Unversioned, bo, boid string, err error) {
	components := strings.SplitN(string(id), ".", 3)
	if len(components) != 3 || components[0] == "" {
		return "", "", "", fmt.Errorf("%q is not a properly composed BusinessWorkflowID, expected wfname.bo.boid", id)
	}
	return Unversioned(components[0]), components[1], components[2], nil
}
