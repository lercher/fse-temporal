package syntax

// Statename describes a workflow state's name by key and display text
type Statename struct {
	Name    Unversioned `json:"name"`    // Name is the key of a workflow state
	Display string      `json:"display"` // Display is a text intended to be shown in a GUI
}
