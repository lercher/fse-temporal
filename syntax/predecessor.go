package syntax

// Predecessor describes source state and events for a target state
type Predecessor struct {
	Source *State
	Events []Event
}
