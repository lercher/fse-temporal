package syntax

// Parameter is either a variable name or a string literal, but not both
type Parameter struct {
	Var  string `json:"var,omitempty"`  // Var is the $name of an interpreter's environment variable
	Text string `json:"text,omitempty"` // Text is the value of a string literal
}
