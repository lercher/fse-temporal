package syntax

import (
	"bufio"
	"bytes"
	"strings"
)

// detabText removes common indentation from a multiline string, single line strings are returned unchanged.
// It must be all indented either with spaces or tabs. If they are mixed
// the behavior is unpredicatable.
// It removes all whitespace incl. new-lines at the beginning and end of the multiline string
// so that the first line is always non-indented and not taken into account for common indentation.
// A multiline string returned always ends with a new-line char.
func detabText(s string) string {
	if strings.IndexByte(s, '\n') == -1 {
		return s
	}
	trimmed := strings.TrimSpace(s)

	// find indentation common to all non-empty lines, ignore first line b/c of TrimSpace()
	countWS := len(trimmed)
	scanner := bufio.NewScanner(strings.NewReader(trimmed))
	isFirstLine := true
	for scanner.Scan() {
		line := []byte(scanner.Text())
		if !isFirstLine && len(line) != 0 && len(bytes.TrimSpace(line)) != 0 {
			// ignore empty or whitespace-only lines for indentaion detection
			n := 0
			for {
				if n >= len(line) {
					break
				}
				if line[n] != ' ' && line[n] != '\t' {
					break
				}
				n++
			}
			if n < countWS {
				countWS = n
			}
		}
		isFirstLine = false
	}
	if countWS == 0 {
		return trimmed // nothing to do
	}

	// line by line remove common indentation
	var buf bytes.Buffer
	scanner = bufio.NewScanner(strings.NewReader(trimmed))
	isFirstLine = true
	for scanner.Scan() {
		line := []byte(scanner.Text())
		if isFirstLine || countWS > len(line) {
			buf.Write(line)
		} else {
			buf.Write(line[countWS:])
		}
		isFirstLine = false
		buf.WriteByte('\n')
	}
	return buf.String()
}
