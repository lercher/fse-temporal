package syntax

import (
	_ "embed"
	"strings"
	"testing"
)

func TestDetabTextSingle(t *testing.T) {
	if got, want := detabText(""), ""; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
	if got, want := detabText("a word"), "a word"; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
	if got, want := detabText("  just a simple string "), "  just a simple string "; got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}

const (
	t1 = "\n\ta\r\n\t\tb\n\t\t\tc\n\ta\n\t"
	w1 = "a\n\tb\n\t\tc\na\n"

	t2 = "  a\r\n    b\n  a\n  a"
	w2 = "a\n  b\na\na\n"

	t3 = "\n            # Mietkauf Übernahme od. Kündigung\n\n            Handelt es sich bei dieser Vertragsbeendigung um eine Übernahme oder eine Kündigung?\n        "
	w3 = "# Mietkauf Übernahme od. Kündigung\n\nHandelt es sich bei dieser Vertragsbeendigung um eine Übernahme oder eine Kündigung?\n"

	// remove starting and trailing empty lines
	t4 = "\n  \n\n            # Mietkauf Übernahme od. Kündigung\n\n            Handelt es sich bei dieser Vertragsbeendigung um eine Übernahme oder eine Kündigung?\n\n   \n        "
	w4 = "# Mietkauf Übernahme od. Kündigung\n\nHandelt es sich bei dieser Vertragsbeendigung um eine Übernahme oder eine Kündigung?\n"
)

func TestDetabTextMultiline(t *testing.T) {
	if got, want := detabText(t1), w1; got != want {
		t.Errorf("\nwant %q\n got %q", want, got)
	}
	if got, want := detabText(t2), w2; got != want {
		t.Errorf("\nwant %q\n got %q", want, got)
	}
	if have, got, want := t3, detabText(t3), w3; got != want {
		t.Errorf("\nhave %q\nwant %q\n got %q", have, want, got)
	}
	if have, got, want := t4, detabText(t4), w4; got != want {
		t.Errorf("\nhave %q\nwant %q\n got %q", have, want, got)
	}
}

//go:embed "detabtext_test.fse"
var strangeIndentationWF string

func TestDescriptionIdentations(t *testing.T) {
	// t.Logf("%q", strangeIndentationWF)
	wf, _, err := Parse(strings.NewReader(strangeIndentationWF), "")
	if err != nil {
		t.Fatal(err) // file should be parseable
	}

	for _, s := range wf.AllStates {
		t.Run(string(s.Name), func(t *testing.T) {
			t.Logf("%q", s.Description)
			if strings.HasPrefix(s.Description, " ") {
				t.Error("state description must not start with a space")
			}
			if strings.HasPrefix(s.Description, "\t") {
				t.Error("state description must not start with a tab")
			}
			if strings.Contains(s.Description, "\n ") {
				t.Error("state description must not contain indentation by space")
			}
			if strings.Contains(s.Description, "\n\t") {
				t.Error("state description must not contain indentation by tab")
			}
		})
	}
}
