package syntax

import (
	"gitlab.com/lercher/fse-temporal/option"
)

// Named is an identifier with a displayname and some descriptive text
type Named struct {
	Name        Unversioned `json:"name"`        // Name is the key of an FSE DSL's object
	Option      string      `json:"option"`      // Option is an option string, that must be parseable via option.Parse
	Display     string      `json:"display"`     // Display is a text intended to be shown in a GUI
	Description string      `json:"description"` // Description is descriptive text attached to an FSE DSL's object. For web GUIs it's normally markdown formatted.
	Roles       []string    `json:"roles"`       // Roles is a list of role names intended to be checked by an RBAC implementation for visibility
}

// DisplayOrName returns Display if it is not empty, otherwise Name is returned
func (n Named) DisplayOrName() string {
	if n.Display != "" {
		return n.Display
	}
	return string(n.Name)
}

// NameOrDisplay returns Name if it is not empty, otherwise Display is returned
func (n Named) NameOrDisplay() string {
	if n.Name != "" {
		return string(n.Name)
	}
	return n.Display
}

// OptionParsed returns Option parsed as option.Option or empty options if invalid
func (n Named) OptionParsed() option.Option {
	o, _ := option.Parse(n.Option)
	return o
}
