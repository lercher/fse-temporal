package syntax

import (
	"strings"
)

// Event is sth a state waits for and can be triggered by Name from the outside
type Event struct {
	Named
	Type         string        `json:"type"`         // Type can be event, delay or initialization
	Parameter    string        `json:"parameter"`    // Parameter is a Duration if this event is of Type delay
	Consequences []Consequence `json:"consequences"` // Consequences is a list of actions to be executed if this event is triggered
}

// IntersectsWith is true if e.Roles is empty or
// otherwise if there is a non-empty intersection between e.Roles
// and the given roles
func (e Event) IntersectsWith(roles []string) bool {
	if len(e.Roles) == 0 {
		// empty "for" list matches all roles
		return true
	}
	for _, r1 := range e.Roles {
		for _, r2 := range roles {
			if r1 == r2 {
				return true
			}
		}
	}
	return false
}

// AllTargets collects all potential states, an event can reach declaratively.
// State changes based on environmental vars are not covered by the result.
func (e Event) AllTargets() (states []string) {
	for _, c := range e.Consequences {
		if c.Goto != "" && !strings.HasPrefix(c.Goto, "$") {
			states = append(states, c.Goto)
		}
	}
	return states
}
