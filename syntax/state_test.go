package syntax

import (
	_ "embed" // only strings
	"strings"
	"testing"
)

//go:embed "sample-workflow-v100.fse"
var sample string

func TestState_Predecessors(t *testing.T) {
	rd := strings.NewReader(sample)
	wf, d, err := Parse(rd, "")
	if err != nil {
		t.Fatal(err, d)
	}

	if got, want := len(wf.States), 4; got != want {
		t.Errorf("want %v top-level states, got %v", want, got)
	}

	if got, want := len(wf.Statenames), 6; got != want {
		t.Errorf("want %v statenames total, got %v", want, got)
	}

	if got, want := len(wf.AllStates), 6; got != want {
		t.Errorf("want %v states total, got %v", want, got)
	}

	s1, ok := wf.AllStates["s1"]
	if !ok {
		t.Fatal("state s1 not found")
	}

	pr := s1.Predecessors(wf)
	if got, want := len(pr), 3; got != want {
		t.Fatalf("want %d predecessors, got %d", want, got)
	}

	if got, want := pr[0].Source.Name, Unversioned("initial"); got != want {
		t.Errorf("want %v, got %v", want, got)
	}
	if got, want := pr[1].Source.Name, Unversioned("inner1"); got != want {
		t.Errorf("want %v, got %v", want, got)
	}
	if got, want := pr[2].Source.Name, Unversioned("test"); got != want {
		t.Errorf("want %v, got %v", want, got)
	}
	if got, want := len(pr[1].Events), 2; got != want {
		t.Errorf("want %v events, got %v", want, got)
	}

	final, ok := wf.AllStates["final"]
	if !ok {
		t.Fatal("state final not found")
	}

	prf := final.Predecessors(wf)
	if got, want := len(prf), 2; got != want {
		t.Fatalf("want %d predecessors, got %d", want, got)
	}
}
