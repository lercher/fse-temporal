package syntax

// Consequence is an action that acts as a consequence of an Initialization, a time-out or an Event
type Consequence struct {
	Line       int         `json:"line"`                 // Line is the linenumber within the source in which this consequence was declared
	Col        int         `json:"col"`                  // Col is the column number within the source in which this consequence was declared
	Action     string      `json:"action"`               // Action is a keyword describing the type of this consequence. It can be assign, funcall, if-goto, goto, if-halt, halt or terminate
	Goto       string      `json:"goto,omitempty"`       // Goto is either the name of a state to go to or the name of an interpreter's environment var containing the target state
	LVar       string      `json:"lvar,omitempty"`       // LVar is the name of en interpreter's $variable used on the left hand side of an assignment or an if clause, its $$ for a map valued action call
	Operator   string      `json:"operator,omitempty"`   // Operator is either == or !=
	Function   string      `json:"function,omitempty"`   // Function is a function's name for a funcall, which is usually mapped to a temporal action name
	Parameters []Parameter `json:"parameters,omitempty"` // Parameters is a list of either a string literal or an interperter's environment variable name
}
