package tpl

import (
	"testing"
)

func TestTemplate(t *testing.T) {
	// must not panic
	defer func() {
		if r := recover(); r != nil {
			t.Error(r)
		}
	}()

	if List == "" || Item == "" {
		t.Fatal("List and Item must not be empty")
	}

	_ = TemplateItem(nil)
	_ = TemplateList()
}
