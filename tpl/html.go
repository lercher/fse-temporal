package tpl

import (
	_ "embed" // only embedding strings
	"html/template"

	"gitlab.com/lercher/fse-temporal/syntax"
)

// Item is a template to render an fse definition to html5/bootstrap4. It uses (( and )) as delimeters.
// See tpl.Template() to get a preconfigured template.
//
//go:embed "item.html"
var Item string

// TemplateItem returns a template called "item" to render an fse definition to html5.
// It also offers a template "embed" which only renders two div-rows containing a header row, and a row with left nav plus content div-cols.
func TemplateItem(wf *syntax.Workflow) *template.Template {
	t := template.New("item").Delims("((", "))").Funcs(map[string]interface{}{"workflow": func() *syntax.Workflow { return wf }})
	t = template.Must(t.Parse(Item))
	return t
}

// List is a template to render a list of fse definitions to html5/bootstrap4. It uses (( and )) as delimeters.
// It needs a
//
//go:embed "list.html"
var List string

// TemplateList returns a template called "list" to render a slice of fse definitions to html5.
// It also offers a template "embed" which only renders the table within the complete page.
func TemplateList() *template.Template {
	t := template.New("list").Delims("((", "))")
	t = template.Must(t.Parse(List))
	return t
}
