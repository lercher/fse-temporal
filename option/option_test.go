package option

import (
	"testing"

	"gitlab.com/lercher/fse-temporal/listener"
)

var o Option

func BenchmarkParseCplx(b *testing.B) {
	// BenchmarkParseCplx-12    	   43864	     27692 ns/op	   13544 B/op	     257 allocs/op
	// BenchmarkParseCplx-4   	       25965	     44178 ns/op	   13480 B/op	     257 allocs/op
	var err error
	for i := 0; i < b.N; i++ {
		o, err = Parse(" abc:something; y12 : value0\tvalue1 ;\nz-4 :'1st value for z'  \" 2nd 'value' for z \"  3rd ; ' ! ':!; hidden;")
		if err != nil {
			b.Error(err)
		}
	}
}

func BenchmarkParse3(b *testing.B) {
	// BenchmarkParse3-12    	   78998	     15604 ns/op	    8104 B/op	     162 allocs/op
	// BenchmarkParse3-4   	       49636	     24537 ns/op	    8072 B/op	     162 allocs/op
	var err error
	for i := 0; i < b.N; i++ {
		o, err = Parse("hidden:no; xyz : 1 2 3; hidden;")
		if err != nil {
			b.Error(err)
		}
	}
}

func BenchmarkParse1(b *testing.B) {
	// BenchmarkParse1-12    	  307692	      3935 ns/op	    2880 B/op	      56 allocs/op
	// BenchmarkParse1-4   	      180292	      6163 ns/op	    2856 B/op	      56 allocs/op
	var err error
	for i := 0; i < b.N; i++ {
		o, err = Parse("hidden")
		if err != nil {
			b.Error(err)
		}
	}
}

func TestParserIsSetAndString(t *testing.T) {
	hiddenParser(t, "hidden", "hidden;")
	hiddenParser(t, "hidden:1", "hidden;")
	hiddenParser(t, "hidden:true", "hidden;")
	hiddenParser(t, "hidden:on", "hidden;")
	hiddenParser(t, "hidden:set", "hidden;")
	hiddenParser(t, "hidden:yes", "hidden;")

	hiddenParser(t, "hidden;", "hidden;")
	hiddenParser(t, "hidden:1;", "hidden;")
	hiddenParser(t, "hidden:true;", "hidden;")
	hiddenParser(t, "hidden :true;", "hidden;")
	hiddenParser(t, "hidden: true;", "hidden;")
	hiddenParser(t, "hidden : true;", "hidden;")
	hiddenParser(t, "hidden:on;", "hidden;")
	hiddenParser(t, "hidden:set;", "hidden;")
	hiddenParser(t, "hidden:yes;", "hidden;")

	hiddenParser(t, "hidden;hidden;", "hidden;")
	hiddenParser(t, "hidden:no;hidden;", "hidden;")
	hiddenParser(t, "hidden:no one ever;hidden;", "hidden;")
	hiddenParser(t, "hidden:no; xyz : 1 2 3; hidden;", "hidden; xyz: 1, 2, 3;")
}

func hiddenParser(t *testing.T, have string, wantString string) {
	t.Helper()
	o, err := Parse(have)
	if err != nil {
		el := err.(*listener.Listener)
		for _, d := range el.Diagnostics {
			t.Error(d)
		}
		t.Errorf("have: %q", have)
		return
	}

	if got, want := o.IsSet("hidden"), true; got != want {
		t.Log(o)
		t.Errorf("IsSet: want %v, got %v", want, got)
	}

	if got, want := o.String(), wantString; got != want {
		t.Errorf("string: want %v, got %v", want, got)
	}
}

func TestParseValues(t *testing.T) {
	have := " abc:something; y12 : value0\tvalue1 ;\nz-4 :'1st value for z'  \" 2nd 'value' for z \"  3rd ; ' ! ':!; hidden; empty:; empty0: [];"
	o, err := Parse(have)
	if err != nil {
		el := err.(*listener.Listener)
		for _, d := range el.Diagnostics {
			t.Error(d)
		}
		t.Fatal()
	}

	t.Log(map[string][]string(o))
	t.Log(o)

	if got, want := o.GetValue("abc"), "something"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.String(), " ! : !; abc: something; empty: []; empty0: []; hidden; y12: value0, value1; z-4: 1st value for z,  2nd 'value' for z , 3rd;"; got != want {
		t.Errorf("\nwant %v,\n got %v", want, got)
	}

	if got, want := o.GetValue("y12"), "[value0 value1]"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue("z-4"), "[1st value for z  2nd 'value' for z  3rd]"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o["z-4"][2], "3rd"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue(" ! "), "!"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue("unknown"), ""; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("unknown"), false; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.GetValue("hidden"), "true"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("hidden"), true; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("abc"), false; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o.IsSet("y12"), false; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := o["empty"], []string{}; len(got) != len(want) {
		t.Errorf("want %v, got %v", want, got)
	}
}

func TestMatch(t *testing.T) {
	k1 := Option{"k": []string{"1"}}
	k2 := Option{"k": []string{"2"}}
	k12 := Option{"k": []string{"1", "2"}}

	l1 := Option{"l": []string{"1"}}
	l2 := Option{"l": []string{"2"}}
	l12 := Option{"l": []string{"1", "2"}}

	k12l12 := Option{"k": []string{"1", "2"}, "l": []string{"1", "2"}}
	k34l34 := Option{"k": []string{"3", "4"}, "l": []string{"3", "4"}}

	wantmatch(t, true, nil, nil)
	wantmatch(t, true, nil, k1)
	wantmatch(t, true, k1, nil)
	wantmatch(t, true, k1, k1)
	wantmatch(t, true, k12l12, k12l12)

	wantmatch(t, false, k1, k2)
	wantmatch(t, true, k12, k2)
	wantmatch(t, true, k1, l1)
	wantmatch(t, true, k12, l12)
	wantmatch(t, true, k2, l2)

	wantmatch(t, true, k1, k12l12)
	wantmatch(t, true, k12l12, k1)
	wantmatch(t, true, k2, k12l12)
	wantmatch(t, true, k12l12, k2)
	wantmatch(t, true, l12, k12l12)
	wantmatch(t, true, k12l12, l12)

	wantmatch(t, false, k12l12, k34l34)
	wantmatch(t, false, k34l34, k12l12)
	wantmatch(t, false, k12, k34l34)
	wantmatch(t, false, k34l34, k12)
	wantmatch(t, false, l12, k34l34)
	wantmatch(t, false, k34l34, l12)
	wantmatch(t, false, l1, k34l34)
	wantmatch(t, false, k34l34, l2)
}

func wantmatch(t *testing.T, ismatch bool, o1, o2 Option) {
	t.Helper()
	got := Match(o1, o2)
	if got != ismatch {
		t.Errorf("want match %v, got %v, have: %v --- %v", ismatch, got, o1, o2)
	}
}

func Test_quote(t *testing.T) {
	tests := []struct {
		name    string
		arg     string
		want    string
		wantErr bool
	}{
		{"empty", ``, `''`, false},
		{"error", `single'double"`, ``, true},
		{"single", `single'`, `"single'"`, false},
		{"double", `double"`, `'double"'`, false},
		{"normal", `normal`, `normal`, false},
		{"123", `123`, `123`, false},
		{"space", `normal 123`, `'normal 123'`, false},
		{"spaces", ` normal  123 `, `' normal  123 '`, false},
		{"bang", `!`, `!`, false},
		{"tab", "\t", "'\t'", false},
		{"cr", "\r", "'\r'", false},
		{"lf", "\n", "'\n'", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := quote(tt.arg)
			if (err != nil) != tt.wantErr {
				t.Errorf("quote() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("quote() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOption_Quoted(t *testing.T) {
	tests := []struct {
		name string
		o    string
		want string
	}{
		{"empty", "", ""},
		{"single", "single:   value ", "single: value;"},
		{"double", "first: 1; second: 2;", "first: 1; second: 2;"},
		{"list", "alpha: a b  c d", "alpha: a b c d;"},
		{"quoted", "'read me': \"now or later\" back then", "'read me': 'now or later' back then;"},
		{"spaces", "' where ': ' no one  had   gone    before '", "' where ': ' no one  had   gone    before ';"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o, err := Parse(tt.o)
			if err != nil {
				t.Fatal(err)
			}
			got, err := o.Quoted()
			if got != tt.want {
				t.Errorf("Option(%s).Quoted() = %v, want %v", tt.o, got, tt.want)
			}
			_, err = Parse(got)
			if err != nil {
				t.Errorf("want: parseable, got error: %v", err)
			}
		})
	}
}
