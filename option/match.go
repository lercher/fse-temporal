package option

// Match returns true, if for all keys that are common to o1 and o2
// their slices share at least one common element or both slices are empty
func Match(o1 Option, o2 Option) bool {
	switch {
	case len(o1) == 0 || len(o2) == 0:
		return true
	case len(o1) <= len(o2):
		for k, list1 := range o1 {
			if list2, ok := o2[k]; ok {
				if !listmatch(list1, list2) {
					return false
				}
			}
		}
		return true
	default:
		for k, list2 := range o2 {
			if list1, ok := o1[k]; ok {
				if !listmatch(list1, list2) {
					return false
				}
			}
		}
		return true
	}
}

func listmatch(l1, l2 []string) bool {
	switch {
	case len(l1) == 0 && len(l2) == 0:
		return true
	case len(l1) == 0 || len(l2) == 0:
		return false // can't have an element in common if only one list is non-empty
	default:
		for _, s1 := range l1 {
			for _, s2 := range l2 {
				if s1 == s2 {
					return true
				}
			}
		}
		return false
	}
}
