package option

import (
	"bytes"
	"errors"
	"fmt"
	"sort"
	"strings"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/lercher/fse-temporal/listener"
	"gitlab.com/lercher/fse-temporal/option/parser"
)

// Option is the result of Parse-ing an option string.
// An option string is of the form:
// `abc: value; y :value0 value1 ; ' z ' : '1st value for z' " 2nd value for z " 3rd; hidden;`
// i.e. a list of key:value value ...; or key; fragments, where key and value can be
// anything except "; : tab space cr lf" or anything quoted with single (') or double (") quoutes.
// If a key appears without a value in the list, it's single value is assumed to be "true".
// Whitespace is trimmed in the first form but not in the quoted form.
// There are no quotes of the same form allowed in the quoted form and there is no escaping.
// In the example above abc is [value], y is [value0 value1], " z " is the list of this three
// strings: "1st value for z" " 2nd value for z " "3rd", note the trimmed and not trimmed key/values.
// hidden is [true], so IsSet("hidden") returns true.
type Option map[string][]string

// ErrCantQuoteString is the error Quoted returns if a key or a value contains both a ' and a "
// which is an un-parseable option.
var ErrCantQuoteString = errors.New("can't quote string")

// Check parses an option string returning Diagnostics
func Check(s string) []listener.Diagnostic {
	_, el := parse(s)
	return el.Diagnostics
}

// Parse parses a new Option map.
// Please note that "last occurrance wins", i.e. if a key is given
// more than once in s, the value list of the last occurrance is considered
// to be *the* value of the key. E.g. "a: a; a: 1 2 3;" returns an Option
// with a=[1 2 3]
func Parse(s string) (Option, error) {
	o, el := parse(s)
	if el.ErrorCount() > 0 {
		return o, el
	}
	return o, nil
}

// parse parses a new Option map
func parse(s string) (Option, *listener.Listener) {
	input := antlr.NewInputStream(s)
	lexer := parser.NewOLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := parser.NewOParser(stream)
	p.Interpreter.SetPredictionMode(antlr.PredictionModeSLL)

	// p has a default error listener that prints unwanted diagnostics, so we remove it first:
	el := listener.New("") // no source
	p.RemoveErrorListeners()
	p.AddErrorListener(el)
	p.BuildParseTrees = true

	// now parse the FSE source reader
	tree := p.Main()
	if el.ErrorCount() > 0 {
		return nil, el
	}

	// create the AST by a walk of the parse tree
	w := newWlk(el, p.GetTokenStream())
	antlr.ParseTreeWalkerDefault.Walk(w, tree)
	if el.ErrorCount() > 0 {
		return nil, el
	}

	return w.option, el
}

// GetValue looks up a particular option value.
// If doesn't exist or it has length zero an empty string is returned.
// If it has length one, the first element is returned.
// If it has length two or more, the slice is Sprint-ed and returned.
// If you need the splice, just use standard map lookup.
func (o Option) GetValue(k string) string {
	vals := o[k]
	switch len(vals) {
	case 0:
		return ""
	case 1:
		return vals[0]
	default:
		return fmt.Sprint(vals)
	}
}

// IsGiven returns true if the key is present in the parsed map.
// The method exists to prevent a possible misinterpretation
// of IsSet() which checks if an option is truthy.
func (o Option) IsGiven(k string) bool {
	_, ok := o[k]
	return ok
}

// IsSet returns true if the list at k is exactly one
// element long and this single value is one of [true 1 on set yes]
func (o Option) IsSet(k string) bool {
	vals := o[k]
	switch len(vals) {
	default:
		return false
	case 1:
		v := vals[0]
		return v == "true" || v == "1" || v == "on" || v == "set" || v == "yes"
	}
}

// String returns a human readable representation of options, not their raw input.
// If you need a re-parseable string use Quoted.
// Keys are sorted as strings and listed in order.
// Lists are represented by unquoted, comma-separated strings.
// Especially boolean true values, see IsSet(), are shown in their key-only form.
func (o Option) String() string {
	keys := make([]string, 0, len(o))
	for k := range o {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var buf bytes.Buffer
	for i, k := range keys {
		if i != 0 {
			buf.WriteString(" ")
		}
		buf.WriteString(k)
		vals := o[k]
		switch len(vals) {
		case 0:
			buf.WriteString(": ")
			buf.WriteString("[]")
		case 1:
			if !o.IsSet(k) {
				buf.WriteString(": ")
				buf.WriteString(vals[0])
			}
		default:
			buf.WriteString(": ")
			for j, v := range vals {
				if j != 0 {
					buf.WriteString(", ")
				}
				buf.WriteString(v)
			}
		}
		buf.WriteString(";")
	}
	return buf.String()
}

// quote returns the given string as is unless it contains [";:\t\r\n ], ' or is empty.
// In the first exceptional and the empty case it is enclosed within ”.
// If it contains ' but no ", it is enclosed within "".
// If it contains both ' and " an error is returned
func quote(s string) (string, error) {
	switch {
	case strings.ContainsRune(s, '"') && strings.ContainsRune(s, '\''):
		return "", ErrCantQuoteString
	case strings.ContainsRune(s, '\''):
		// can't contain " here
		return `"` + s + `"`, nil
	case strings.ContainsAny(s, "\";:\t\r\n "):
		// can't contain ' here
		return `'` + s + `'`, nil
	case s == "":
		return `''`, nil
	default:
		return s, nil
	}
}

// Quoted returns a normalized parseable string represention
// of the Option. Boolean true flags, see IsSet, reproduce their original
// string value and "true" in case it was left out.
// It returns ErrCantQuoteString and the offending key/value, if the
// Option contains a key or a value that both contains a single and
// a double quote, which is not allowed by the parser but can be
// constructed via the standard map functionality of Option.
func (o Option) Quoted() (string, error) {
	keys := make([]string, 0, len(o))
	for k := range o {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var buf bytes.Buffer
	for i, k := range keys {
		if i != 0 {
			buf.WriteString(" ")
		}
		qk, err := quote(k)
		if err != nil {
			return k, err
		}
		buf.WriteString(qk)
		buf.WriteString(": ")

		vals := o[k]
		for j, v := range vals {
			if j != 0 {
				buf.WriteString(" ")
			}
			qv, err := quote(v)
			if err != nil {
				return v, err
			}
			buf.WriteString(qv)
		}
		buf.WriteString(";")
	}
	return buf.String(), nil
}
